<?php

const IMPORT_DIRECTORY = __DIR__ .'/../imports/ldlh/';
const CHILD_DIRECTORY = __DIR__ .'/../imports/';
const EXPORT_DIRECTORY = __DIR__ .'/../exports/';

const FINANCE_CHECK_LIST = [1, 2, 3, 4, 5, 6, 7, 8];
const FINANCE_CHECK_THRESHOLD = 0.05;

const INVOICE_DATE_CHECK = '2021-04-27';
const FINANCIAL_YEAR_START_DATE = '2020-10-03';

// Files being used
const FILES_DATE = '2021-04-27';
const FOLDER = 'go-live/';
const EXPORT_PROPERTIES = FOLDER . 'sykes-Lake-District-Lodge-Holidays-export-properties_' . FILES_DATE . '.csv';
const PROPERTIES_EXTRA = FOLDER . '2359-properties.csv';
const EXPORT_BOOKINGS = FOLDER . 'sykes-Lake-District-Lodge-Holidays-export-bookings_' . FILES_DATE . '.csv';
const BOOKING_DETAILS = FOLDER . '2359-booking_details.csv';
const BOOKINGS = FOLDER . '2359-bookings.csv';
const BOOKING_VOUCHERS = FOLDER . '2359-booking_vouchers.csv';
const BOOKING_PAYMENTS = FOLDER . 'sykes-Lake-District-Lodge-Holidays-export-booking-payments_' . FILES_DATE . '.csv';
const BOOKING_FINANCES = FOLDER . 'sykes-Lake-District-Lodge-Holidays-export-booking-finances_' . FILES_DATE . '.csv';
const KNOWN_BOOKINGS_FILE = 'known.csv';
const ERROR_CHECK_FILE = 'error_check.csv';
const OWNER_PAYMENTS = FOLDER . 'sykes-Lake-District-Lodge-Holidays-export-owner-payments_' . FILES_DATE . '.csv';
const BOOKING_OPTIONS = FOLDER . '2359-booking_options.csv';
const AGENT_PAYMENTS = FOLDER . '2359-booking_payments_agents.csv';

$currentErrorCheck = (object)[
    'check1' => 0.0,
    'check2' => 0.0,
    'check3' => 0.0,
    'check4' => 0.0,
    'check5' => 0.0,
    'check6' => 0.0,
    'check7' => 0.0,
    'check8' => 0.0,
    'checkDate' => date('Y-m-d H:i'),
    'folder' => FOLDER,
];

$totalErrorCheck = (object)[
    'check1' => 0,
    'check2' => 0,
    'check3' => 0,
    'check4' => 0,
    'check5' => 0,
    'check6' => 0,
    'check7' => 0,
    'check8' => 0,
    'checkDate' => date('Y-m-d H:i'),
];

$bookingErrorCheck = (object)[
    'check1' => [],
    'check2' => [],
    'check3' => [],
    'check4' => [],
    'check5' => [],
    'check6' => [],
    'check7' => [],
    'check8' => [],
    'checkDate' => date('Y-m-d H:i'),
];


$financeErrorChecks = fopen(CHILD_DIRECTORY.ERROR_CHECK_FILE, 'rb');

if (!$financeErrorChecks) {
    die('Could not open file: "'.CHILD_DIRECTORY.ERROR_CHECK_FILE.'"');
}

$headers = fgetcsv($financeErrorChecks);

$lastErrorCheck = [];
while (($row = fgetcsv($financeErrorChecks)) !== false) {
    $row = (object) array_combine($headers, $row);
    $lastErrorCheck = $row;
}

if (empty($lastErrorCheck)) {
    $lastErrorCheck = $currentErrorCheck;
}

$dodgyBookings = [
//    Bookings failing Test 1
//    38356, // payment matches refund and booking is cancelled
//    39352, // booking cancelled following sale of lodge
//    42255, // Cancelled booking - 20£ difference
//    41363, // sale of property
//    37168, // security_deposit was moved from previous booking / last year
//    40271, // With Change of Ownership
//    42107, // Total Cost should match the interface after reducing paid Security Deposit,
//    43113, // Customer Overpaid (repeat payment) - Refunded to balance the account - payment Date < booking end date
//    42602, // Dogs as Owner Payment
//    44235, // ! Check with Scott
//    44372, // ! Check with Scott
//    44686, // ! Check with Scott

    // Booking failing Test 2
    29142, // Booking has been moved and owner payment has been taken back and customer has been refunded too. This affects the total amount due to the owner
    37866, // Everything matches for this booking but still we are left with £10  -- share with jon
    38211, // Change of Ownership, two owners paid the same amount.  Payment to an owner who doesnt own the property
    38292, // Change of Ownership, Payment to an owner who doesnt own the property
    38326, // Change of Ownership, Payment to an owner who doesnt own the property
    38356, // Change of ownership, payment to owner who doesnt own the property and no refund from that owner
    37193, // Booking cancelled but not refunded
    38684, // The booking total cost is wrong. Also there is an option where the optionprice for the Dogs is zero.
    40602, // Leftover £48.75 on the Formula. the variable values seems to match the interface

    // Booking failing Test 7
    38357, // Change of ownership, payment to owner who doesnt own the property and no refund from that owner,
    40286, // Owner not refunded
    40847, // Owner not refunded
    43587, // Owner not refunded
    43647, // Owner not refunded
    44226, // Owner not refunded
    44309, // Owner not refunded
    44808, // Owner not refunded
    44844, // Owner not refunded
    45458, // Owner not refunded
    46603, // Owner not refunded
    46713, // Owner not refunded
    47074, // Owner not refunded
    47101, // Owner not refunded
    47139, // Owner not refunded
    47377, // Owner not refunded

    // Booking failing Test 5
    33797, // Booking cancelled, gift voucher used, customer not refunded as voucher used and owner also not refunded
    33805, // Booking cancelled, gift voucher used, customer not refunded as voucher used and owner also not refunded
    33806, // Booking cancelled, gift voucher used, customer not refunded as voucher used and owner also not refunded
    33808, // Booking cancelled, gift voucher used, customer not refunded as voucher used and owner also not refunded
    33865, // Booking cancelled, gift voucher used, customer not refunded as voucher used and owner also not refunded
    34985, // Booking cancelled, gift voucher used, customer not refunded as voucher used and owner also not refunded
    38979, // Different commission rate used when generating owner invoices. Causing 18£ difference
    39432, // With leftover 50£. Supercontrol needs checking.
    39540, // Owner booking with customer payments
    39799, // Gross Commmission Rental and Owern Amount Due matches but test fails
    39816, // Rental matches but commission and owner amount due does not due to dog
    39820, // Payments not cleared from previous owner
    39895, // No future invoices has been raised for this property

    37042, // Booking cancelled, customer paid, owner not refunded

    // Booking failing Test 4
    46454, // Customer Not refunded fully, left over funds
    47983, // Customer and owner Not refunded.
    48281, // Customer Not refunded fully, left over funds
    38358,

    // Bookings failing Test 3
    47165, // Booking is Cancelled on the 22-03 Data (referring to 21-03), but has been reinstated on the 22-03
    48526, // Owner Booking with "Owner Admin Service"
    48527, // Owner Booking with "Owner Admin Service"
    51706, // Transferred booking from 41427. New booking Total Price is not making sense

    // Bookings failing Test 8
    38422, // Multi booking [Cancelled + Confirmed], Grand Total price on the data is wrong
];
$propertyRef = [];
$properties = fopen(IMPORT_DIRECTORY.EXPORT_PROPERTIES, 'rb');

if (!$properties) {
    die('Could not open file: "'.IMPORT_DIRECTORY.EXPORT_PROPERTIES.'"');
}

$headers = fgetcsv($properties);

while (($row = fgetcsv($properties)) !== false) {
    $row = (object) array_combine($headers, $row);
    $propertyRef[$row->cottageID] = (object) [
        'cottageID' => $row->cottageID,
        'managerID' => $row->managerID,
        'cottagename' => $row->cottagename,
        'property_status' => $row->property_status,
        'commission_rate' => $row->commission_rate
    ];
}

$propertiesExtraData = fopen(IMPORT_DIRECTORY.PROPERTIES_EXTRA, 'r');

if (!$propertiesExtraData) {
    die('Could not open file: "'.IMPORT_DIRECTORY.PROPERTIES_EXTRA.'"');
}

$headers = fgetcsv($propertiesExtraData);

while (($row = fgetcsv($propertiesExtraData)) !== false) {
    $row = (object) array_combine($headers, $row);
    $propertyRef[$row->cottageID]->rcvoldID = $row->rcvoldID;
}

$bookingOptionsArray = [];
$bookingOptions = fopen(IMPORT_DIRECTORY . BOOKING_OPTIONS, 'r');

if (!$bookingOptions) {
    die('Could not open file: "' . IMPORT_DIRECTORY . BOOKING_OPTIONS . '"');
}

$headers = fgetcsv($bookingOptions);

while (($row = fgetcsv($bookingOptions)) !== false) {
    $row = (object)array_combine($headers, $row);
    $bookingOptionsArray[$row->bookingdetailID][] = $row;
}

$bookingdetails = fopen(IMPORT_DIRECTORY . BOOKING_DETAILS, 'r');

if (!$bookingdetails) {
    die('Could not open file: "' . IMPORT_DIRECTORY . BOOKING_DETAILS . '"');
}

$headers = fgetcsv($bookingdetails);

$shortRef = [];
$longRef = [];

while (($row = fgetcsv($bookingdetails)) !== false) {
    $row = (object)array_combine($headers, $row);

    if (!isset($longRef[$row->bookingID])) {
        $longRef[$row->bookingID] = (object)[];
    }

    $longRef[$row->bookingID]->details[$row->bookingdetailID] = $row;
    $longRef[$row->bookingID]->details[$row->bookingdetailID]->options = isset($bookingOptionsArray[$row->bookingdetailID]) ? $bookingOptionsArray[$row->bookingdetailID] : [];
}

$bookings = fopen(IMPORT_DIRECTORY.EXPORT_BOOKINGS, 'r');

if (!$bookings) {
    die('Could not open file: "' . IMPORT_DIRECTORY . EXPORT_BOOKINGS . '"');
}

$headers = fgetcsv($bookings);

while (($row = fgetcsv($bookings)) !== false) {
    $row = (object)array_combine($headers, $row);

    $row->finances = [];
    $row->property = [];
    $row->vouchers = [];
    $row->payments = [];
    $row->ownerpayments = [];
    $row->type ='Unknown';

    if (empty($longRef[$row->bookingID]->details)) {
        $row->details = [];
    }
    if (!empty($longRef[$row->bookingID])) {
        $longRef[$row->bookingID] = (object)array_merge((array)$row, (array)$longRef[$row->bookingID]);
        $shortRef[$row->owner_bookingID] = $longRef[$row->bookingID];
    }
}

$voucherData = fopen(IMPORT_DIRECTORY.BOOKING_VOUCHERS, 'r');

if (!$voucherData) {
    die('Could not open file: "'.IMPORT_DIRECTORY.BOOKING_VOUCHERS.'"');
}

$headers = fgetcsv($voucherData);

while (($row = fgetcsv($voucherData)) !== false) {
    $row = (object) array_combine($headers, $row);
    if (isset($longRef[$row->bookingID])) {
        $longRef[$row->bookingID]->vouchers [] = $row;
    }
}

$payment = fopen(IMPORT_DIRECTORY.BOOKING_PAYMENTS, 'r');

if (!$payment) {
    die('Could not open file: "'.IMPORT_DIRECTORY.BOOKING_PAYMENTS.'"');
}

$headers = fgetcsv($payment);

while (($row = fgetcsv($payment)) !== false) {
    $row = (object) array_combine($headers, $row);
    if (isset($longRef[$row->bookingID])) {

        $longRef[$row->bookingID]->payments[] = $row;
    }

}

$ownerPayments = fopen(IMPORT_DIRECTORY . OWNER_PAYMENTS, 'r');

if (!$ownerPayments) {
    die('Could not open file: "' . IMPORT_DIRECTORY . OWNER_PAYMENTS . '"');
}

$headers = fgetcsv($ownerPayments);

while (($row = fgetcsv($ownerPayments)) !== false) {
    $row = (object)array_combine($headers, $row);
    if (isset($longRef[$row->bookingID])) {

        $longRef[$row->bookingID]->ownerpayments[] = $row;
    }
}

$finances = fopen(IMPORT_DIRECTORY.BOOKING_FINANCES, 'r');

if (!$finances) {
    die('Could not open file: "'.IMPORT_DIRECTORY.BOOKING_FINANCES.'"');
}

$headers = fgetcsv($finances);

while (($row = fgetcsv($finances)) !== false) {
    $row = (object) array_combine($headers, $row);
    if (isset($shortRef[$row->owner_bookingID])) {
        $shortRef[$row->owner_bookingID]->finances[] = $row;

        if (!empty($propertyRef[$row->cottageID])) {
            $shortRef[$row->owner_bookingID]->property = $propertyRef[$row->cottageID];
        }
    }
}

$bookingInfo = fopen(IMPORT_DIRECTORY . BOOKINGS, 'r');
$headers = fgetcsv($bookingInfo);

while (($row = fgetcsv($bookingInfo)) !== false) {
    $row = (object)array_combine($headers, $row);
    if (isset($longRef[$row->bookingID])) {
        $longRef[$row->bookingID]->type = $row->bookingtype;
    }
}

$agentPayments = fopen(IMPORT_DIRECTORY . AGENT_PAYMENTS, 'r');
$headers = fgetcsv($agentPayments);

while (($row = fgetcsv($agentPayments)) !== false) {
    $row = (object)array_combine($headers, $row);
    if (isset($longRef[$row->bookingID])) {
        $longRef[$row->bookingID]->paymentagent[] = $row;
    }
}

$knownFile = fopen(CHILD_DIRECTORY.KNOWN_BOOKINGS_FILE, 'r');

if (!$knownFile) {
    die('Could not open file: "'.CHILD_DIRECTORY.KNOWN_BOOKINGS_FILE.'"');
}

$headers = fgetcsv($knownFile);
$known = [];

while (($row = fgetcsv($knownFile)) !== false) {
    $row = array_combine($headers, $row);

//    $known[$row['Booking Ref']] = $row;
}

if (empty($argv[1])) {
    $outputFileName = 'filtered-bookings.' . date('Ymd_Hi') . '.csv';
    $out = fopen(EXPORT_DIRECTORY . $outputFileName, 'wb');

    if (!$out) {
        die('Could not open file: "' . IMPORT_DIRECTORY . $outputFileName . '"');
    }
}

$newHeaders = [
    'Booking Ref',
    'Total Cost',
    'Total Rent',
    'Booking Fee',
    'Extras',
    'Charity Donation',
    'SD',
    'Due to owner RENT',
    'Due to owner EXTRAS',
    'Total Due to owner',
    'Paid to owner',
    'OWNER LIABILITY',
    'Total to LDLH',
    'Comm Gross',
    'Comm Net',
    'Comm VAT',
    'Extras Comm Gross',
    'Extras Comm Net',
    'Extras Comm VAT',
    'Booking Fee 2', // requested by Jon
    'Total Received From Customer',
    'Total Relating to Holiday',
    'Balance Due on Holiday',
    'Total SD Due',
    'SD Paid',
    'SD Refunded',
    'Balance of SD Held',
    'Status',
    'Property ref',
    'Owner ref',
    'Holiday booked date',  // This booked date will be wrong for multibookings as we don't have the information
                            // for when the new booking have been created
    'Holiday start date',
    'Holiday end date',
    'Source',
    'Comm%',
    'Cottage Name',
    'Cottage Short Name',
    'Booking Type',
    'Discount',
    'Booking Protection Charge',
    'Multi Booking Ref',
    'Multi Booking Count',
    'Other Charges',
    'Voucher',
    'Payments To More than 1 Owner',
    'Admin Charge',
//    'Debtors Control Account',
//    'Sykes Deferred',
//    'Bank Current Account',
//    'Owner Deferred',
//    'Owner Balances',
//    'Sykes Rec Income',
    'Amount Due to Owner (today)', // requested by Jon
    'Amount Due to LDLH (today)', // requested by Jon
];

if (empty($argv[1])) {
    fputcsv($out, $newHeaders);
}
foreach ($shortRef as $booking) {
    $multiBookingNumber = 0;
    ksort($booking->details);

    foreach ($booking->details as $number => $bookingDetail) {
        $latestDetail = end($booking->details);
        $booking->bookingenddate = $latestDetail->bookingenddate;

        if (filterBookings($booking, $bookingDetail)) {
            continue;
        }

        if (!isSameCottage($booking, $bookingDetail)) {
            $booking->bookingstatus = $bookingDetail->bookingstatus;
        }

        $ownerDeferred = getDeferredToOwner($booking, $bookingDetail);
        if ($booking->total_price <= 0) {
            //its cancelled, and fully refunded, take all of the money back
            $ownerDeferred = -getSettledToOwner($booking, $bookingDetail);
        }
        $commissionPercentage = 0;
        if ($booking->total_rent > 0) {
            $commissionPercentage = $booking->property->commission_rate ?? @getCommissionNet($booking, $bookingDetail) / ($booking->total_rent);
        }

        $sdPaid = getSDPaid($booking) ?: getSD($booking);
        $sdRefunded = getSDRefunded($booking);
        if ($sdRefunded < 0) {
            $sdPaid = 0;
        }
        $sdHeld = 0;
        if ($sdPaid > 0 && $sdRefunded == 0) {
            $sdHeld = $sdPaid;
        }

        $totalBookingCost = getBookingTotalCost($booking, $bookingDetail) - shouldDeductSecurityDeposit($bookingDetail, $sdPaid);
        $bookingDetailTotalCost = getBookingDetailTotalCost($booking, $bookingDetail);
        $totalRental = getBookingDetailRent($booking, $bookingDetail);
        $totalBookingFee = getBookingFee($booking, $bookingDetail);
        $otherCharges = getOtherCharges($booking, $bookingDetail);
        $adminCharge = getAdminCharge($booking, $bookingDetail);

        $amountCustomerRefunded = getCustomerRefunded($booking);
        $amountCustomerPaid = getCustomerPaid($booking);
        $totalReceivedFromCustomer = 0;
        $totalRelatingToHoliday = 0;


        $splitCustomerAmountPaidForBookingDetail = $amountCustomerPaid + $amountCustomerRefunded + $booking->voucher;
        if (count($booking->details) > 1) {
            $splitCustomerAmountPaidForBookingDetail = splitCustomerAmountPaidForBookingDetail($totalBookingCost, $bookingDetailTotalCost, $totalRelatingToHoliday);
        }
        $balanceDueOnHoliday = 0;
        if (!isBookingCancelled($bookingDetail)) {
            $totalReceivedFromCustomer = $amountCustomerPaid - ($amountCustomerRefunded * -1);
            $totalRelatingToHoliday = $totalReceivedFromCustomer > $bookingDetailTotalCost ? $totalReceivedFromCustomer - $sdPaid : $totalReceivedFromCustomer;
            $balanceDueOnHoliday = $bookingDetailTotalCost - $splitCustomerAmountPaidForBookingDetail;
        }

        //Commission and VAT
        $totalCommissionNet = getCommissionNet($booking, $bookingDetail);
        $totalCommissionExtraVAT = getCommissionVat($booking, $bookingDetail);
        $grossCommission = $totalCommissionNet + $totalCommissionExtraVAT;

        $netExtrasCommission = getExtrasNet($booking, $bookingDetail);
        $netExtrasCommissionVAT = getExtrasVAT($booking, $bookingDetail);
        $grossNetCommissionExtras = $netExtrasCommission + $netExtrasCommissionVAT;

        $bookingProtectionCharge = getBookingProtectionCharge($booking, $bookingDetail);

        // Still not working for the example booking: 46697
        $totalDueToOwner = getTotalDueToOwner($booking, $ownerDeferred, $bookingDetail);
        $paymentToMoreThanOneOwner =
            (($totalDueToOwner > $bookingDetailTotalCost) || checkBookingPaymentsToMoreThanOneOwner($booking)) ?
            'yes' : '';

        $cutOffDate = DateTime::createFromFormat('Y-m-d', INVOICE_DATE_CHECK)->format('Y-m-d H:i:s');

        $totalToLDLH = getTotalToLDLH($booking, $bookingDetail);
        $getFutureAmountsDueToLDLH = getFutureAmountsDueToLDLH($booking->finances, $cutOffDate);
        $amountDueToOwnerToday = $totalDueToOwner - getFutureAmountsDueToOwner($booking->finances, $cutOffDate);
        $amountDueToLDLHToday = $totalToLDLH - $getFutureAmountsDueToLDLH;

        $multiBookingNumberRef = '';
        if ($number > 0) {
            $multiBookingNumberRef = $number;
        }

        $multiBookingNumberText = '';
        if ($multiBookingNumber !== 0) {
            $multiBookingNumberText = '-' . $multiBookingNumber;
        }
        $multiBookingNumber++;

        $row = [
            $booking->owner_bookingID,                                      // 'Booking Ref'
            $bookingDetailTotalCost,                                        // 'Total Cost'
            $totalRental,                                                   // 'Total Rent'
            $totalBookingFee,                                               // 'Booking Fee'
            getExtrasTotal($booking, $bookingDetail),                       // Extras - Dog Example: £10
            getCharityDonation($booking, $bookingDetail),                   // 'Charity Donation'
            getSD($booking),                                                // Security Deposit (SD)
            getRentDueToOwner($booking, $bookingDetail),                    // 'Due to owner RENT'
            getOwnerExtras($booking, $bookingDetail),                       // Due to owner EXTRAS: Extras Dog Example: £8
            $totalDueToOwner,                                               // Total Due to Owner
            getSettledToOwner($booking, $bookingDetail),                    // Paid to Owner
            $ownerDeferred,                                                 // 'OWNER LIABILITY'
            $totalToLDLH,                                                   // Total to LDLH
            $grossCommission,                                               // 'Comm Gross'
            $totalCommissionNet,                                            // 'Comm Net'
            $totalCommissionExtraVAT,                                       // 'Comm VAT'
            $grossNetCommissionExtras,                                      // Extras Dog Example: £2
            $netExtrasCommission,                                           // Extras Dog Example: £1.67
            $netExtrasCommissionVAT,                                        // Extras Dog Example: £0.33
            $totalBookingFee,                                               // 'Booking Fee'
            $splitCustomerAmountPaidForBookingDetail,                       // 'Total Received From Customer'
            $totalRelatingToHoliday,                                        // 'Total Relating to Holiday'
            $balanceDueOnHoliday,                                           // 'Balance Due on Holiday'
            $booking->security_deposit_amount,                              // 'Total SD Due'
            $sdPaid,                                                        // 'SD Paid'
            $sdRefunded,                                                    // 'SD Refunded'
            $sdHeld,                                                        // 'Balance of SD Held'
            $bookingDetail->bookingstatus,                                  // 'Status'
            $bookingDetail->cottageID ,                                     // 'Property ref'
            $booking->property->managerID ?? '',                            // 'Owner ref'
            $booking->bookingdate,                                          // 'Holiday booked date'
            $bookingDetail->bookingstartdate,                               // 'Holiday start date'
            $bookingDetail->bookingenddate,                                 // 'Holiday end date'
            $booking->sourceID,                                             // 'Source'
            $commissionPercentage,                                          // 'Comm%'
            $propertyRef[$bookingDetail->cottageID]->cottagename ?? '',     // 'Cottage Name'
            $propertyRef[$bookingDetail->cottageID]->rcvoldID ?? '',        // 'Cottage Short Name'
            $booking->type,                                                 // 'Booking Type'
            getBookingDiscount($bookingDetail),                             // 'Discount'
            $bookingProtectionCharge,                                       // 'Booking Protection Charge'
            $multiBookingNumberRef,                                         // 'Multi Booking Ref'
            $booking->owner_bookingID . $multiBookingNumberText,            // 'Multi Booking Count'
            $otherCharges,                                                  // 'Other Charges' Exclude Booking Protection and Booking Fee'
            $booking->voucher,                                              // 'Voucher'
            $paymentToMoreThanOneOwner,                                     // 'Payments To More than 1 Owner'
            $adminCharge,                                                   // 'Admin Charge'
//            $bookingDetailTotalCost - $totalReceivedFromCustomer,           // 'Debtors Control Account'
//            $getFutureAmountsDueToLDLH,                                     // 'Sykes Deferred'
//            $totalReceivedFromCustomer,                                     // 'Bank Current Account'
//            $totalDueToOwner - (float)$booking->paid_to_owner,              // 'Owner Deferred'
//            (float)($booking->paid_to_owner ?? 0),                          // 'Owner Balances'
//            getSykesReceivedIncome($booking, $totalReceivedFromCustomer, $bookingDetailTotalCost, $totalDueToOwner),   // 'Sykes Rec Income'
            $amountDueToOwnerToday,                                         // 'Amount Due to Owner (today)' - requested by Jon
            $amountDueToLDLHToday,                                          // 'Amount Due to LDLH (today)' - requested by Jon
        ];

        $row = array_combine($newHeaders, $row);

        if (!empty($argv[1]) && !in_array($argv[1], FINANCE_CHECK_LIST) && $booking->owner_bookingID == $argv[1]) {
            print_r([
                'booking_array' . (!empty($number) ? (' -> multibooking: ' . $number . PHP_EOL) : ''),
                $booking
            ]);

            echo PHP_EOL .
                'CSV Row (goes on the file)' .
                PHP_EOL .
                (!empty($number) ? (' -> multibooking: ' . $number . PHP_EOL) : '');

            print_r($row);

            print_r([
                'sykes deferred' => $getFutureAmountsDueToLDLH,
                'bookingDetailTotalCost' => $bookingDetailTotalCost,
                'splitCustomerAmountPaidForBookingDetail' => $splitCustomerAmountPaidForBookingDetail,
                'totalDueToOwner' => $totalDueToOwner,
            ]);
            echo PHP_EOL;

            if ($number) {
                echo '--------------' . PHP_EOL . '--------------' . PHP_EOL . PHP_EOL;
            }
        }

        // Finance Checks
        // Skip bookings that relates to multiple properties AND that are on the $dodgyBookings list
        if (!empty($argv[1]) && strpos($booking->cottageID, ',') === false && !in_array($booking->owner_bookingID, $dodgyBookings)) {
            // index interval: [1 - 8]
            runFinanceChecks((int)$argv[1], $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
        }

        // Sum the formulas of Finance checks and Sum all rows to build the total
        sumFinanceChecks($row, $currentErrorCheck, $totalErrorCheck, $bookingErrorCheck);

        // TODO build up a list of Known bookings
//    if (isset($known[$booking->owner_bookingID])) {
//        $checkColumns = $known[$booking->owner_bookingID];
//        $currentBooking = array_combine($newHeaders, $row);
//        $checkArray = array_intersect_key($currentBooking, $checkColumns);
//        $diff = array_diff($checkColumns, $checkArray);
//        if ($diff) {
//            foreach($diff as $key => $val) {
//                echo "Booking {$booking->owner_bookingID}".PHP_EOL;
//                echo "Expecting $key = {$checkColumns[$key]}".PHP_EOL;
//                echo "Got $key = {$checkArray[$key]}".PHP_EOL;
//                echo PHP_EOL;
//            }
//            die();
//        }
//    }

        if (empty($argv[1])) {
            fputcsv($out, $row);
        }
    }
}

if (empty($argv[1])) {
    fclose($out);
    echo 'Saved file: ' . $outputFileName . PHP_EOL . PHP_EOL;

    compareFinanceChecks($currentErrorCheck, $lastErrorCheck);

    // Save the new error check
    appendDataToErrorCheck(IMPORT_DIRECTORY . ERROR_CHECK_FILE, $currentErrorCheck);
//    outPutErrorReport($totalErrorCheck,$bookingErrorCheck);
}

function filterBookings($booking, $bookingDetail)
{
    return $booking->bookingenddate < FINANCIAL_YEAR_START_DATE
//        || $booking->type === 'OWNER'
    ;
}

function getRefundedAmountIfBookingIsMoved($booking){
    foreach ($booking->finances as $finance){
        if (stripos($finance->paymentcaption,'balance refunded')){
            return $finance->owner_due + $finance->commission;
        }
    }
    return 0;
}

function isDepositFromPreviousOwner($financeRow)
{
    $paymentCaptions = [
        'deposit from previous owner',
        'deposit to new owner',
        'balance from previous owner',
        'payment to new owner',
        'dates moved',
    ];

    return multi_stripos($financeRow->paymentcaption, $paymentCaptions);
}

function isBookingCancelled($booking)
{
    return $booking->bookingstatus === 'cancelled';
}

function isBalanceToOwner($financeRow)
{
    return stripos($financeRow->paymentcaption, 'balance to owner') !== false;
}

function isDepositToOwner($financeRow)
{
    return stripos($financeRow->paymentcaption, 'deposit to owner') !== false;
}

function getTotalDueToOwner($booking, $ownerLiability, $bookingDetail)
{
    if (isBookingCancelled($bookingDetail)) {
        return 0;
    }

    return getSettledToOwner($booking, $bookingDetail) + $ownerLiability;
}

function shouldDeductSecurityDeposit($booking, $sdPaid)
{
    if (isBookingCancelled($booking)) {
        return 0;
    }
    return $sdPaid;
}


function getBookingDetailRent($booking, $bookingDetail)
{
    if (isBookingCancelled($bookingDetail)) {
        return 0;
    }

    $bookingFullRate = $bookingDetail->bookingfullrate;
    $rent = $bookingFullRate - getBookingDiscount($bookingDetail);

    foreach($booking->vouchers as $row) {
        if ($row->BOOKINGvoucher_owner_pay === 'before') {
            $rent -= $booking->voucher;
        }
    }

    return $rent - getAgentsCommission($booking, $bookingDetail);
}

function getBookingRent($booking){
    if (isBookingCancelled($booking)) {
        return 0;
    }

    $bookingRent = $booking->total_rent;
    foreach($booking->vouchers as $row) {
        if ($row->BOOKINGvoucher_owner_pay === 'before') {
            $bookingRent -= $booking->voucher;
        }
    }

    return $bookingRent;
}

function getBookingTotalCost($booking, $bookingDetail)
{
    if (isBookingCancelled($bookingDetail)) {
        return 0;
    }

    $totalCost = $booking->total_price;
    $rent = getBookingRent($booking);
    $extras = getExtrasTotal($booking, $bookingDetail);
    $charityDonation = getCharityDonation($booking, $bookingDetail);

    $securityDeposit = getSDRefunded($booking) < 0 ? getSDRefunded($booking) : getSd($booking);
    $bookingFee = getBookingFee($booking, $bookingDetail);
    $bookingProtectionCharge = getBookingProtectionCharge($booking, $bookingDetail);
    $otherCharges = getOtherCharges($booking, $bookingDetail);

    $voucherAmount = $booking->voucher;

    $expectedTotalCost = $rent + $bookingFee + $otherCharges + $bookingProtectionCharge + $extras + $charityDonation + $securityDeposit - $voucherAmount;

    $totalCost += getRefundedAmountsToGetOriginalCostPrice($expectedTotalCost, $totalCost, $booking);
    $totalCost += getVoucherAmountToGetOriginalCostPrice($booking, $totalCost);

    return $totalCost;
}

function shouldReturnFinances($booking, $bookingDetail)
{
    $sumOfPayments = getSumOfPayments($booking, $bookingDetail);
    if (isBookingCancelled($bookingDetail) && $sumOfPayments <= 0) {
        return false;
    }
    return true;
}

function getBookingDetailTotalCost($booking, $bookingDetail)
{
    if (isBookingCancelled($bookingDetail)) {
        return 0;
    }

    $bookingFullRate = $bookingDetail->bookingfullrate;
    $rent = $bookingFullRate - getBookingDiscount($bookingDetail);
    $extras = getExtrasTotal($booking, $bookingDetail);
    $charityDonation = getCharityDonation($booking, $bookingDetail);

    $bookingFee = getBookingFee($booking, $bookingDetail);
    $bookingProtectionCharge = getBookingProtectionCharge($booking, $bookingDetail);
    $otherCharges = getOtherCharges($booking,$bookingDetail);
    $adminCharge = getAdminCharge($booking, $bookingDetail);
    $agentsCommission = getAgentsCommission($booking, $bookingDetail);

    $voucherAmount = $booking->voucher;

    $totalCost = $rent
        + $bookingFee
        + $extras
        + $bookingProtectionCharge
        + $otherCharges
        + $charityDonation
        + $adminCharge
        - $voucherAmount
        - $agentsCommission
    ;

    $totalCost += getVoucherAmountToGetOriginalCostPrice($booking, $totalCost);

    return $totalCost;
}

function getBookingDiscount($bookingDetail){
    return
        $bookingDetail->bookingdiscount_late +
        $bookingDetail->bookingdiscount_multi +
        $bookingDetail->bookingdiscount_couples +
        $bookingDetail->bookingfixeddiscount +
        $bookingDetail->bookingdiscount_xy +
        $bookingDetail->bookingotherdiscount;
}

function splitCustomerAmountPaidForBookingDetail($totalBookingCost, $bookingDetailTotalCost, $totalRelatingToHoliday)
{
    /*
$booking1 - £700
$booking2 - £62

Grand Total = £762
-------------------
Customer Paid= £135

Split amounts:  (CustPayment / Total cost) * BookingTotalCost
$booking1 - (£135 / £762) * £700 = £124.02
$booking2 - (£135 / £762) * £62  = £10.98
 */
    if ($totalBookingCost == 0 ||$bookingDetailTotalCost  == 0) {
        return 0;
    }

    if ($bookingDetailTotalCost == $totalRelatingToHoliday) {
        return $totalBookingCost;
    }

    return ($totalRelatingToHoliday / $totalBookingCost) * $bookingDetailTotalCost;
}

function getVoucherAmountToGetOriginalCostPrice($booking, $totalCost)
{
    $voucherAmount = 0;
    foreach($booking->vouchers as $row) {
        // booking ID 41952 => total_price already has voucher applied
        if ($row->BOOKINGvoucher_owner_pay === 'none') {
            $voucherAmount += $booking->voucher;
        }

        // booking ID 42026 => Mismatch between interface and local data
        if ($row->BOOKINGvoucher_owner_pay === 'commission') {
            if (isVoucherAlreadyDeductedFromTotalCost($totalCost, $booking->total_rent, $booking->voucher)) {
                $voucherAmount += $booking->voucher;
            } else {
                $voucherAmount -= $booking->voucher;
            }
        }
    }

    return $voucherAmount;
}


function getRefundedAmountsToGetOriginalCostPrice($expectedTotalCost, $totalCost, $booking)
{
    $refundedAmounts = [];
    $refundTotal = 0;
    if ($expectedTotalCost > $totalCost) {
        foreach ($booking->payments as $payment) {
            if ($payment->paymenttotal < 0) {
                $refundedAmounts[] = $payment->paymenttotal;
            }
        }

        foreach ($booking->payments as $payment) {
            $positiveMatchingAmount = $payment->paymenttotal * -1;

            if (in_array($positiveMatchingAmount, $refundedAmounts)) {
                foreach ($refundedAmounts as $key => $refundedAmount) {
                    if ($refundedAmount === (float)$positiveMatchingAmount) {
                        unset($refundedAmounts[$key]);
                    }
                }
            }
        }

        if (!empty($refundedAmounts)) {
            foreach ($refundedAmounts as $amount) {
                $refundTotal += -1 * $amount;
            }
        }
    }

    return $refundTotal;
}

function isVoucherAlreadyDeductedFromTotalCost($totalCost, $rentalAmount, $voucherAmount)
{
    if (($totalCost - $voucherAmount) < $rentalAmount + $voucherAmount) {
        return true;
    }
    return false;
}

function isOwnerPayment($paymentCaption)
{
    $paymentCaptions = [
        'to owner',
        'balance refunded',
        'dates moved',
        'covid',
        'refund due',
        'revious owner',
        'new owner',
        'carried forward', //booking deposit carried forward to future booking
        'booking cancelled',
        'dog cancelled', // 43808 when dog extra is cancelled then use the original dog extra vat and amount
        'commission adjust', // 44226 commission adjustment from previous owner for bookings
        'fully managed',
        'change of date - booking',
        'part refund',
        'moved', // 45540 moved from previous owner
        'pro rata refund',
        'additional payment',
        'admin fee',
    ];

    return multi_stripos($paymentCaption, $paymentCaptions);
}

function isCommissionAdjustment($paymentCaption)
{
    $paymentCaptions = ['fully managed', 'commission adjusted', 'change of date'];
    return multi_stripos($paymentCaption, $paymentCaptions);
}

function getRentDueToOwner($booking, $bookingDetail) {
    if (isBookingCancelled($bookingDetail)) {
        return 0;
    }

    $rent = 0;
    foreach($booking->finances as $row) {
        if (isOwnerPayment($row->paymentcaption) && isSameCottage($row,$bookingDetail)) {
            $rent += $row->owner_due;
        }
    }

    return $rent;
}

function getExtrasTotal($booking, $bookingDetail) {
    if (isBookingCancelled($bookingDetail)) {
        return 0;
    }

    return getOwnerExtras($booking, $bookingDetail) +
        getExtrasNet($booking, $bookingDetail) +
        getExtrasVAT($booking, $bookingDetail);
}

function getOwnerExtras($booking, $bookingDetail) {
    if (isBookingCancelled($bookingDetail)){
        return 0;
    }

    $extras = 0;

    foreach($booking->finances as $row) {
        if (!isOwnerPayment($row->paymentcaption) && isSameCottage($row, $bookingDetail)) {
            $extras += $row->owner_due;
        }
    }

//    $extras -= amendOwnerExtras($bookingDetail);

    return $extras;
}

function amendOwnerExtras($bookingDetail)
{
    if (isBookingCancelled($bookingDetail)){
        return 0;
    }

    $extrasAmend = 0;

    foreach ($bookingDetail->options as $option) {
        if ($option->optionprice == 0) {
            $extrasAmend += $option->ownerprice;
        }
    }

    return $extrasAmend;
}

function getExtrasNet($booking, $bookingDetail) {
    $extras = 0;

    if (!shouldReturnFinances($booking, $bookingDetail)) {
        return 0;
    }

    foreach($booking->finances as $row) {
        if (!isOwnerPayment($row->paymentcaption) && isSameCottage($row,$bookingDetail)) {
            // Ignore the different column name
            // Extras Dog Example: £1.67
            $extras += $row->other_extras_vat;
        }
    }
    return $extras;
}

function getExtrasVAT($booking, $bookingDetail) {
    $extras = 0;

    if (!shouldReturnFinances($booking, $bookingDetail)) {
        return 0;
    }

    foreach($booking->finances as $row) {
        if (!isOwnerPayment($row->paymentcaption) && isSameCottage($row, $bookingDetail)) {
            // Ignore the different column name
            // Extras Dog Example: £0.33
            $extras += $row->other_extras_net;
        }
    }
    return $extras;
}

function getBookingFee($booking, $bookingDetail)
{
    if (!shouldReturnFinances($booking, $bookingDetail)) {
        return 0;
    }

    $fee = 0;
    foreach ($booking->finances as $row) {
        if (isSameCottage($row, $bookingDetail) && stripos($row->paymentcaption, 'booking fee') !== false) {
            $fee += $row->booking_fee + $row->booking_fee_vat;
        }
    }
    return $fee;
}

function getCommissionNet($booking, $bookingDetail)
{
    $commissionNet = 0;
    $ownerDeposits = [];
    $previousOwnerDeposits = [];

    if (!shouldReturnFinances($booking, $bookingDetail)) {
        return 0;
    }

    foreach ($booking->finances as $row) {
        if (isOwnerPayment($row->paymentcaption) && isSameCottage($row, $bookingDetail)) {
            if (isDepositToOwner($row)) {
                $ownerDeposits []= $row;
            }
            if (isDepositFromPreviousOwner($row)) {
                $previousOwnerDeposits [] = $row;
            }

            $commissionNet += $row->commission;
        }
    }

    $missingCommission = deductMissingCommission($ownerDeposits, $previousOwnerDeposits);
    return $commissionNet - $missingCommission;
}

function getCommissionVat($booking, $bookingDetail) {
    $fee = 0;
    $ownerDeposits=[];
    $previousOwnerDeposits=[];

    if (!shouldReturnFinances($booking, $bookingDetail)) {
        return 0;
    }

    foreach ($booking->finances as $row) {
        if (isOwnerPayment($row->paymentcaption) && isSameCottage($row, $bookingDetail)) {
            if (isDepositToOwner($row)) {
                $ownerDeposits []= $row;
            }
            if (isDepositFromPreviousOwner($row)) {
                $previousOwnerDeposits [] = $row;
            }
            $fee += $row->commission_vat;
        }
    }
    $missingCommissionVAT = deductMissingCommissionVAT($ownerDeposits, $previousOwnerDeposits);
    return $fee - $missingCommissionVAT;
}

function getSumOfPayments($booking, $bookingDetail = null, $ignoreBookingStatus = false)
{
    if (!$ignoreBookingStatus && isBookingCancelled($bookingDetail)) {
        return 0;
    }

    $total = 0;
    foreach ($booking->payments as $payment) {
        $total += $payment->paymenttotal;
    }

    return $total;
}

function getTotalToLDLH($booking, $bookingDetail) {
    $total = 0;
    $ownerDeposits = [];
    $previousOwnerDeposits = [];

    $ignoreBookingStatus = false;
    if (count($booking->details) === 1) {
        $ignoreBookingStatus = true;
    }

    $sumOfPayments = getSumOfPayments($booking, $bookingDetail, $ignoreBookingStatus);

    if (isBookingCancelled($bookingDetail) && $sumOfPayments <= 0) {
        return 0;
    }

    foreach ($booking->finances as $row) {
        if (isSameCottage($row, $bookingDetail)) {
            $total += $row->commission + $row->commission_vat +
                $row->booking_fee + $row->booking_fee_vat +
                $row->admin_fee +
                $row->other_extras_net + $row->other_extras_vat;

            if (isDepositToOwner($row)) {
                $ownerDeposits []= $row;
            }
            if (isDepositFromPreviousOwner($row)) {
                $previousOwnerDeposits [] = $row;
            }
        }
    }

    $total -= getCharityDonation($booking, $bookingDetail);
    $total -= deductMissingCommission($ownerDeposits, $previousOwnerDeposits);
    $total -= deductMissingCommissionVAT($ownerDeposits, $previousOwnerDeposits);

    return $total;
}

function getSettledToOwner($booking, $bookingDetail)
{
    $settled = 0;
    $propertyOwnerId = $booking->property->managerID ?? 0;
    foreach ($booking->finances as $row) {
        if (isOwnerPaidForRaisedInvoice($row, $booking->ownerpayments, $propertyOwnerId) && isSameCottage($row, $bookingDetail)) {
            $settled += $row->owner_due;
        }
    }
    return $settled;
}

function getDeferredToOwner($booking, $bookingDetail)
{
    // Change of ownership, when two owner have been paid, then disregard the old owner and check for the new one instead
    // When a normal booking, then check for the owner

    $deferred = 0;
    $propertyOwnerID = $booking->property->managerID ?? 0;
    $ownerIds = getOwnerIDFromPayments($booking->ownerpayments);

    foreach ($booking->finances as $row) {

        if (isOwnerPaidForRaisedInvoice($row, $booking->ownerpayments, $propertyOwnerID) && isSameCottage($row, $bookingDetail)) {
            continue;
        }

        if (/**in_array($propertyOwnerID, $ownerIds) &&*/
            !isOwnerPaidForRaisedInvoice($row, $booking->ownerpayments, $propertyOwnerID) &&
            isSameCottage($row, $bookingDetail)) {
            $deferred += $row->owner_due;
        }
    }

    // booking 42347
    if (isBookingCancelled($bookingDetail)) {
        return getSettledToOwner($booking, $bookingDetail) * -1;
    }

    return $deferred;
}

function isOwnerPaidForRaisedInvoice($financeRow, $ownerPayments, $propertyOwnerId)
{
    $paymentExists = false;

    foreach ($ownerPayments as $ownerPayment) {
        $paidDate = date('Y-m-d', strtotime($ownerPayment->paiddate));
        if (
            $ownerPayment->managerID === $propertyOwnerId &&
            $ownerPayment->owner_payment === $financeRow->owner_due &&
            isSamePaymentCaption($financeRow, $ownerPayment) &&
            $paidDate < INVOICE_DATE_CHECK
        ) {
            $paymentExists = true;
        }
    }
    return $paymentExists;
}

function getCustomerPaid($booking) {
    $paid = 0;
    foreach($booking->payments as $payment) {
        if ($payment->paymenttotal > 0) {
            $paid += $payment->paymenttotal;
        }
    }

    return $paid;
}

function getCustomerRefunded($booking) {
    $refunded = 0;
    foreach($booking->payments as $payment) {
        if ($payment->paymenttotal < 0) {
            $refunded += $payment->paymenttotal;
        }
    }

    return $refunded;
}

function getSD($booking) {
    return $booking->security_deposit_amount;
}

function getSDPaid($booking) {
    if (!empty($booking->security_deposit_paid)) {
        return $booking->security_deposit_paid;
    }

    return 0;
}

function getSDRefunded($booking) {
    $security_deposit_paid = (getSDPaid($booking) ?: getSD($booking));

    if (!empty($booking->payments) && $security_deposit_paid > 0) {
        foreach ($booking->payments as $payment) {
            if ( // $payment->paymentdate >= $booking->bookingenddate &&
                (float)$payment->paymenttotal <= (-1 * getSDPaid($booking) ?: -1 * getSD($booking))) {
                return -1 * $security_deposit_paid;
            }
        }
    }

    return 0;
}

function getCharityDonation($booking, $bookingDetail) {
    $donation = 0;

    if (isBookingCancelled($bookingDetail)) {
        return 0;
    }

    if (!empty($booking->finances)) {
        foreach($booking->finances as $row ) {
            if (preg_match('/^Fix the Fells - please donate/', $row->paymentcaption) && isSameCottage($row, $bookingDetail)) {
                $donation += $row->booking_fee_vat;
            }
        }
    }

    return $donation;
}

function financeCheck($checkSum) {
    return abs($checkSum) < FINANCE_CHECK_THRESHOLD;
}

function runFinanceChecks($index, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    // Total Cost - Rent +Extras + Bk Fee + Charity Donation
    if ($index === 1) { // =B3-C3-D3-E3-F3
        $financeCheck = ($row['Total Cost'] - $row['Total Rent'] - $row['Booking Fee'] - $row['Extras'] - $row['Charity Donation'] - $row['Booking Protection Charge'] - $row["Other Charges"] - $row['Admin Charge']);
        financeCheckOne($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    // Total Cost - Total To owner + Total to LDLH
    if ($index === 2) { // =B3-J3-M3
        $financeCheck = ($row['Total Cost'] - $row['Total Due to owner'] - $row['Total to LDLH'] - $row['Charity Donation']);
        financeCheckTwo($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    // Owner Total - Rent + Extras
    if ($index === 3) { // =J3-I3-H3
        $financeCheck = ($row['Total Due to owner'] - $row['Due to owner EXTRAS'] - $row['Due to owner RENT']);
        financeCheckThree($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    // LDLH Total - Comm Rent + Comm Extras + Bk Fee
    if ($index === 4) { // =M3-N3-Q3-T3
        $financeCheck = ($row["Total to LDLH"] - $row["Comm Gross"] - $row["Extras Comm Gross"] - $row["Booking Fee"] - $row["Booking Protection Charge"] - $row["Other Charges"] - $row['Admin Charge']);
        financeCheckFour($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    // Rent - Owner Rent + Comm Rent
    if ($index === 5) { // =C3-H3-N3
        $financeCheck = ($row['Total Rent'] - $row['Due to owner RENT'] - $row['Comm Gross']);
        financeCheckFive($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    // Total Extras - Owner Extras - LDLH Extras
    if ($index === 6) { // =E3-I3-Q3
        $financeCheck = ($row['Extras'] - $row['Due to owner EXTRAS'] - $row['Extras Comm Gross']);
        financeCheckSix($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    // Total to Owner - Total Paid -Total Due
    if ($index === 7) { // =J3-K3-L3
        $financeCheck = ($row['Total Due to owner'] - $row['Paid to owner'] - $row['OWNER LIABILITY']);
        financeCheckSeven($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    // Total Cost - Received from Customer - Balance due
    if ($index === 8) { // =B3-V3-X3
        $financeCheck = ($row['Total Cost'] - $row['Total Received From Customer'] - $row['Balance Due on Holiday']);
        financeCheckEight($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }
}

function financeCheckOne($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    // skip when difference is the same amount as the voucher
    $voucher = $booking->voucher;
    $voucherCheck = abs($voucher) - abs($financeCheck);

    if (
        (!financeCheck($financeCheck) && $voucherCheck != 0) &&
        !($voucher >= 0 && $booking->total_price <= 0)
    ) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 01 failed: ' => $booking->owner_bookingID,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'voucherCheck' => $voucherCheck,
                'voucher' => $voucher,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '($row["Total Cost"] - $row["Total Rent"] - $row["Booking Fee"] - $row["Extras"] - $row["Charity Donation"]) - $row["Booking Protection Charge"] - $row["Other Charges"] - $row["Admin Charge"]',
                'Total Cost' => $row['Total Cost'],
                'Total Rent' => (float)$row['Total Rent'],
                'Booking Fee' => $row['Booking Fee'],
                'Extras' => $row['Extras'],
                'Charity Donation' => $row['Charity Donation'],
                'Booking Protection Charge' => $row['Booking Protection Charge'],
                'Other Charges' => $row['Other Charges'],
                'Admin Charge' => $row['Admin Charge'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function financeCheckTwo($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    if (
        !financeCheck($financeCheck)
    ) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 02 failed: ' => $booking->owner_bookingID,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '$row["Total Cost"] - $row["Total Due to owner"] - $row["Total to LDLH"]',
                'Total Cost' => $row['Total Cost'],
                'Total Due to owner' => $row['Total Due to owner'],
                'Total to LDLH' => $row['Total to LDLH'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function financeCheckThree($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    if (
    !financeCheck($financeCheck)
    ) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 03 failed: ' => $booking->owner_bookingID,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '$row["Total Due to owner"] - $row["Due to owner EXTRAS"] - $row["Due to owner RENT"]',
                'Total Due to owner' => $row['Total Due to owner'],
                'Due to owner EXTRAS' => $row['Due to owner EXTRAS'],
                'Due to owner RENT' => $row['Due to owner RENT'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function financeCheckFour($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    if (
    !financeCheck($financeCheck)
    ) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 04 failed: ' => $booking->owner_bookingID,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '$row["Total to LDLH"] - $row["Comm Gross"] - $row["Extras Comm Gross"] - $row["Booking Fee"] - $row["Booking Protection Charge"] - $row["Other Charges"] - $row["Admin Charge"]',
                'Total to LDLH' => $row['Total to LDLH'],
                'Comm Gross' => $row['Comm Gross'],
                'Extras Comm Gross' => $row['Extras Comm Gross'],
                'Booking Fee' => $row['Booking Fee'],
                'Booking Protection Charge' => $row['Booking Protection Charge'],
                'Other Charges' => $row['Other Charges'],
                'Admin Charge' => $row['Admin Charge'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function financeCheckFive($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    if (
    !financeCheck($financeCheck)
    ) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 05 failed: ' => $booking->owner_bookingID,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '$row["Total Rent"] - $row["Due to owner RENT"] - $row["Comm Gross"]',
                'Total Rent' => $row['Total Rent'],
                'Due to owner RENT' => $row['Due to owner RENT'],
                'Comm Gross' => $row['Comm Gross'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function financeCheckSix($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    if (
    !financeCheck($financeCheck)
    ) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 06 failed: ' => $booking->owner_bookingID,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '$row["Extras"] - $row["Due to owner EXTRAS"] - $row["Extras Comm Gross"]',
                'Extras' => $row['Extras'],
                'Due to owner EXTRAS' => $row['Due to owner EXTRAS'],
                'Extras Comm Gross' => $row['Extras Comm Gross'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function financeCheckSeven($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    if (
    !financeCheck($financeCheck)
    ) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 07 failed: ' => $booking->owner_bookingID,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '$row["Total Due to owner"] - $row["Paid to owner"] - $row["OWNER LIABILITY"]',
                'Total Due to owner' => $row['Total Due to owner'],
                'Paid to owner' => $row['Paid to owner'],
                'OWNER LIABILITY' => $row['OWNER LIABILITY'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function financeCheckEight($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    if (
    !financeCheck($financeCheck)
    ) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 08 failed: ' => $booking->owner_bookingID,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '$row["Total Cost"] - $row["Total Received From Customer"] - $row["Balance Due on Holiday"]',
                'Total Cost' => $row['Total Cost'],
                'Total Received From Customer' => $row['Total Received From Customer'],
                'Balance Due on Holiday' => $row['Balance Due on Holiday'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function appendDataToErrorCheck($fileName, $fileData) {
    $fileStream = fopen($fileName, 'a');

    if (!$fileStream) {
        echo PHP_EOL.'ERROR - Could not open file "'.$fileName.'"'.PHP_EOL;
        return;
    }

    // Write data
    fputcsv($fileStream, get_object_vars($fileData));

    fclose($fileStream);
}

function sumFinanceChecks($row, $currentErrorCheck, $totalErrorCheck, $bookingErrorCheck)
{
    $financeCheck1 = ($row['Total Cost'] - $row['Total Rent'] - $row['Booking Fee'] - $row['Extras'] - $row['Charity Donation'] - $row['Booking Protection Charge'] - $row['Other Charges'] - $row['Admin Charge']);
    $financeCheck2 = ($row['Total Cost'] - $row['Total Due to owner'] - $row['Total to LDLH'] - $row['Charity Donation']);
    $financeCheck3 = ($row['Total Due to owner'] - $row['Due to owner EXTRAS'] - $row['Due to owner RENT']);
    $financeCheck4 = ($row['Total to LDLH'] - $row['Comm Gross'] - $row['Extras Comm Gross'] - $row['Booking Fee'] - $row['Booking Protection Charge'] - $row['Other Charges'] - $row['Admin Charge']);
    $financeCheck5 = ($row['Total Rent'] - $row['Due to owner RENT'] - $row['Comm Gross']);
    $financeCheck6 = ($row['Extras'] - $row['Due to owner EXTRAS'] - $row['Extras Comm Gross']);
    $financeCheck7 = ($row['Total Due to owner'] - $row['Paid to owner'] - $row['OWNER LIABILITY']);
    $financeCheck8 = ($row['Total Cost'] - $row['Total Received From Customer'] - $row['Balance Due on Holiday']);

    $totalErrorCheck->check1 += financeCheck($financeCheck1) ? 0 : 1;
    $totalErrorCheck->check2 += financeCheck($financeCheck2) ? 0 : 1;
    $totalErrorCheck->check3 += financeCheck($financeCheck3) ? 0 : 1;
    $totalErrorCheck->check4 += financeCheck($financeCheck4) ? 0 : 1;
    $totalErrorCheck->check5 += financeCheck($financeCheck5) ? 0 : 1;
    $totalErrorCheck->check6 += financeCheck($financeCheck6) ? 0 : 1;
    $totalErrorCheck->check7 += financeCheck($financeCheck7) ? 0 : 1;
    $totalErrorCheck->check8 += financeCheck($financeCheck8) ? 0 : 1;


    if (!financeCheck($financeCheck1)) $bookingErrorCheck->check1[] =$row['Booking Ref'];
    if (!financeCheck($financeCheck2)) $bookingErrorCheck->check2[] =$row['Booking Ref'];
    if (!financeCheck($financeCheck3)) $bookingErrorCheck->check3[] =$row['Booking Ref'];
    if (!financeCheck($financeCheck4)) $bookingErrorCheck->check4[] =$row['Booking Ref'];
    if (!financeCheck($financeCheck5)) $bookingErrorCheck->check5[] =$row['Booking Ref'];
    if (!financeCheck($financeCheck6)) $bookingErrorCheck->check6[] =$row['Booking Ref'];
    if (!financeCheck($financeCheck7)) $bookingErrorCheck->check7[] =$row['Booking Ref'];
    if (!financeCheck($financeCheck8)) $bookingErrorCheck->check8[] =$row['Booking Ref'];

    $currentErrorCheck->check1 += abs($financeCheck1);
    $currentErrorCheck->check2 += abs($financeCheck2);
    $currentErrorCheck->check3 += abs($financeCheck3);
    $currentErrorCheck->check4 += abs($financeCheck4);
    $currentErrorCheck->check5 += abs($financeCheck5);
    $currentErrorCheck->check6 += abs($financeCheck6);
    $currentErrorCheck->check7 += abs($financeCheck7);
    $currentErrorCheck->check8 += abs($financeCheck8);
}

function compareFinanceChecks($currentErrorCheck, $lastErrorCheck)
{
    echo 'Error check comparison:' . PHP_EOL;

    $differenceCheck1 = round(abs($currentErrorCheck->check1) - $lastErrorCheck->check1, 2);
    $differenceCheck2 = round(abs($currentErrorCheck->check2) - $lastErrorCheck->check2, 2);
    $differenceCheck3 = round(abs($currentErrorCheck->check3) - $lastErrorCheck->check3, 2);
    $differenceCheck4 = round(abs($currentErrorCheck->check4) - $lastErrorCheck->check4, 2);
    $differenceCheck5 = round(abs($currentErrorCheck->check5) - $lastErrorCheck->check5, 2);
    $differenceCheck6 = round(abs($currentErrorCheck->check6) - $lastErrorCheck->check6, 2);
    $differenceCheck7 = round(abs($currentErrorCheck->check7) - $lastErrorCheck->check7, 2);
    $differenceCheck8 = round(abs($currentErrorCheck->check8) - $lastErrorCheck->check8, 2);
    $differenceFolders = $currentErrorCheck->folder !== $lastErrorCheck->folder ? "{$currentErrorCheck->folder}" : 'no difference';

    var_export([
        'Last error check' => [
            'check1' => financeCheck($lastErrorCheck->check1) ? 'pass' : $lastErrorCheck->check1,
            'check2' => financeCheck($lastErrorCheck->check2) ? 'pass' : $lastErrorCheck->check2,
            'check3' => financeCheck($lastErrorCheck->check3) ? 'pass' : $lastErrorCheck->check3,
            'check4' => financeCheck($lastErrorCheck->check4) ? 'pass' : $lastErrorCheck->check4,
            'check5' => financeCheck($lastErrorCheck->check5) ? 'pass' : $lastErrorCheck->check5,
            'check6' => financeCheck($lastErrorCheck->check6) ? 'pass' : $lastErrorCheck->check6,
            'check7' => financeCheck($lastErrorCheck->check7) ? 'pass' : $lastErrorCheck->check7,
            'check8' => financeCheck($lastErrorCheck->check8) ? 'pass' : $lastErrorCheck->check8,
            'checkDate' => $lastErrorCheck->checkDate,
            'folder' => $lastErrorCheck->folder,
        ],
        'Current error check' => [
            'check1' => financeCheck($currentErrorCheck->check1) ? 'pass' : round($currentErrorCheck->check1, 2),
            'check2' => financeCheck($currentErrorCheck->check2) ? 'pass' : round($currentErrorCheck->check2, 2),
            'check3' => financeCheck($currentErrorCheck->check3) ? 'pass' : round($currentErrorCheck->check3, 2),
            'check4' => financeCheck($currentErrorCheck->check4) ? 'pass' : round($currentErrorCheck->check4, 2),
            'check5' => financeCheck($currentErrorCheck->check5) ? 'pass' : round($currentErrorCheck->check5, 2),
            'check6' => financeCheck($currentErrorCheck->check6) ? 'pass' : round($currentErrorCheck->check6, 2),
            'check7' => financeCheck($currentErrorCheck->check7) ? 'pass' : round($currentErrorCheck->check7, 2),
            'check8' => financeCheck($currentErrorCheck->check8) ? 'pass' : round($currentErrorCheck->check8, 2),
            'checkDate' => $currentErrorCheck->checkDate,
            'folder' => $currentErrorCheck->folder
        ],
        'Difference' => [
            'check1' => financeCheck($differenceCheck1) ? 'no difference' : $differenceCheck1,
            'check2' => financeCheck($differenceCheck2) ? 'no difference' : $differenceCheck2,
            'check3' => financeCheck($differenceCheck3) ? 'no difference' : $differenceCheck3,
            'check4' => financeCheck($differenceCheck4) ? 'no difference' : $differenceCheck4,
            'check5' => financeCheck($differenceCheck5) ? 'no difference' : $differenceCheck5,
            'check6' => financeCheck($differenceCheck6) ? 'no difference' : $differenceCheck6,
            'check7' => financeCheck($differenceCheck7) ? 'no difference' : $differenceCheck7,
            'check8' => financeCheck($differenceCheck8) ? 'no difference' : $differenceCheck8,
            'folder' => $differenceFolders
        ]
    ]);
}

function getBookingProtectionCharge($booking, $bookingDetail)
{
    if (!shouldReturnFinances($booking, $bookingDetail)) {
        return 0;
    }

    $protectionAmount = 0;
    $paymentCaptions = ['refund protection', 'protect premium', 'booking protect'];
    foreach ($bookingDetail->options as $row) {
        if (multi_stripos($row->optionname, $paymentCaptions)) {
            $protectionAmount += $row->optiontotal;
        }
    }
    return $protectionAmount;
}

function getAdminCharge($booking, $bookingDetail)
{
    if (!shouldReturnFinances($booking, $bookingDetail)) {
        return 0;
    }

    $adminCharge = 0;
    $paymentCaptions = ['admin', 'Administration', 'part payment fee', 'change of dates and lodge', 'covid19 - change of dates'];
    foreach ($bookingDetail->options as $row) {
        if (multi_stripos($row->optionname, $paymentCaptions)) {
            $adminCharge += $row->optiontotal;
        }
    }
    return $adminCharge;
}

function getOtherCharges($booking, $bookingDetail)
{
    $paymentCaptions = ['booking fee', 'refund protection', 'protect premium', 'booking protect', 'admin', 'admin_fee', 'administration', 'part payment fee', 'change of dates and lodge', 'covid19 - change of dates'];
    $sumOfPayments = getSumOfPayments($booking, $bookingDetail);
    $bookingProtectionCharge = getBookingProtectionCharge($booking, $bookingDetail);

    if (isBookingCancelled($bookingDetail) && ($sumOfPayments <= 0 || $sumOfPayments === $bookingProtectionCharge)) {
        return 0;
    }

    $fee = 0;
    foreach ($booking->finances as $row) {
        if (isSameCottage($row, $bookingDetail) && !multi_stripos($row->paymentcaption, $paymentCaptions)) {
            $fee += $row->booking_fee + $row->booking_fee_vat;
        }
    }
    return $fee - getCharityDonation($booking, $bookingDetail);
}

function checkBookingPaymentsToMoreThanOneOwner($booking)
{
    $ownerPaymentsSum = [];
    foreach ($booking->ownerpayments as $ownerpayment) {
        if (empty($ownerPaymentsSum[$ownerpayment->managerID])) {
            $ownerPaymentsSum[$ownerpayment->managerID] = (object) [
                'owner_payment' => $ownerpayment->owner_payment,
                'ownerID' => $ownerpayment->managerID,
            ];
        } else {
            $ownerPaymentsSum[$ownerpayment->managerID]->owner_payment += $ownerpayment->owner_payment;
        }
    }

    // - check if the Owner payment amounts match: this means that one of the owners have not refunded the moneys
    // - if the Owner payment amounts DON'T match: then check IF one of the amounts is > zero and < the highest amount
    // => return TRUE if then above conditions are a match
    $maxPaymentInArray = maxValueInArray($ownerPaymentsSum, 'owner_payment', 'ownerID');
    foreach ($ownerPaymentsSum as $ownerPayment) {
        // For every Payment to a different Owner, check if the payment amounts match ->
        // If so: this means that one of the owners have not refunded the moneys
        if ($ownerPayment->ownerID !== $maxPaymentInArray->ownerID &&
            $ownerPayment->owner_payment === $maxPaymentInArray->owner_payment) {
            return true;
        }

        // If the Owner payment amounts DON'T match:
        // Then check IF one of the amounts is > zero and < the highest amount
        if ($ownerPayment->ownerID !== $maxPaymentInArray->ownerID &&
            $ownerPayment->owner_payment !== $maxPaymentInArray->owner_payment &&
            $ownerPayment->owner_payment > 0 && $ownerPayment->owner_payment < $maxPaymentInArray->owner_payment) {
            return true;
        }
    }

    return false;
}

function getSykesDeferred($bookingDetailTotalCost, $balanceDueToOwner)
{
    // Sykes deferred will be the
    // = [Total cost of holiday] - [Balance due to owner]
    // ( the allocation order for the other fees is what is unclear for the deferred )
    return $bookingDetailTotalCost - $balanceDueToOwner;
}

function maxValueInArray($array, $keyToSearch, $keyToIdentify)
{
    $currentMax = null;
    $currentMaxKey = null;
    foreach($array as $arr)
    {
        foreach($arr as $key => $value)
        {
            if ($key == $keyToSearch && ($value >= $currentMax))
            {
                $currentMax = $value;
                $currentMaxKey = $arr->{$keyToIdentify};
            }
        }
    }

    return (object)[
        $keyToSearch => $currentMax,
        $keyToIdentify => $currentMaxKey,
    ];
}

function getAgentsCommission($booking, $bookingDetail)
{
    if (!shouldReturnFinances($booking, $bookingDetail)) {
        return 0;
    }

    if (!empty($booking->third_party_agent)) {
        return $booking->third_party_agent;
    }

    if (!empty($booking->paymentagent)) {
        foreach ($booking->paymentagent as $agentCommission) {
            if ($agentCommission->customerdiscount == 1) {
                return $agentCommission->commission;
            }
        }
    }

    return 0;
}

function getOwnerIDFromPayments($ownerPayments)
{
    $ids = [];
    foreach ($ownerPayments as $ownerPayment) {
        if (!in_array($ownerPayment->managerID, $ids)) {
            $ids[] = $ownerPayment->managerID;
        }
    }
    return $ids;
}

function multi_stripos($string, $check)
{
    $check = (array) $check;
    foreach ($check as $s) {
        $pos = stripos($string, $s);
        if ($pos !== false) {
            return true;
        }
    }
    return false;
}

function deductMissingCommission($newOwnerDeposits, $previousOwnerDeposits)
{
    foreach ($newOwnerDeposits as $newOwnerDeposit) {
        foreach ($previousOwnerDeposits as $previousOwnerDeposit) {
            if ((float)$newOwnerDeposit->owner_due === abs((float)$previousOwnerDeposit->owner_due)) {
                if ($previousOwnerDeposit->commission == 0 && $previousOwnerDeposit->commission_vat == 0) {
                    return (float)$newOwnerDeposit->commission ;
                }
            }
        }
    }
    return 0;
}

function deductMissingCommissionVAT($newOwnerDeposits, $previousOwnerDeposits)
{
    foreach ($newOwnerDeposits as $newOwnerDeposit) {
        foreach ($previousOwnerDeposits as $previousOwnerDeposit) {
            if ((float)$newOwnerDeposit->owner_due === abs((float)$previousOwnerDeposit->owner_due)) {
                if ($previousOwnerDeposit->commission == 0 && $previousOwnerDeposit->commission_vat == 0) {
                    return (float)$newOwnerDeposit->commission_vat ;
                }
            }
        }
    }
    return 0;
}

function getFutureAmountsDueToOwner($bookingFinances, $todayDate) {
    $totalFutureAmount = 0.0;

    foreach ($bookingFinances as $bookingFinance) {
        if ($bookingFinance->invoice_date > $todayDate) {
            $totalFutureAmount += $bookingFinance->owner_due;
        }
    }

    return $totalFutureAmount;
}

function getFutureAmountsDueToLDLH($bookingFinances, $todayDate) {
    $totalFutureAmount = 0.0;

    foreach ($bookingFinances as $bookingFinance) {
        if ($bookingFinance->invoice_date > $todayDate) {
            $totalFutureAmount +=
                $bookingFinance->commission_vat +
                $bookingFinance->commission +
                $bookingFinance->booking_fee +
                $bookingFinance->booking_fee_vat +
                $bookingFinance->admin_fee +
                $bookingFinance->other_extras_net +
                $bookingFinance->other_extras_vat;
        }
    }

    return $totalFutureAmount;
}

function getSykesReceivedIncome($booking, $totalReceivedFromCustomer, $bookingDetailTotalCost, $totalDueToOwner) {
    if (isBookingFullyPaid($totalReceivedFromCustomer, $bookingDetailTotalCost)) {
        return $totalReceivedFromCustomer - $totalDueToOwner;
    }

    $paidToOwner = (float)($booking->paid_to_owner ?? 0);

    return $totalReceivedFromCustomer - $paidToOwner;
}

function isBookingFullyPaid($totalReceivedFromCustomer, $bookingDetailTotalCost) {
    if ($totalReceivedFromCustomer >= $bookingDetailTotalCost) {
        return true;
    }

    return false;
}

function isSameCottage($row, $bookingDetail)
{
    return $row->cottageID == $bookingDetail->cottageID;
}

function isSamePaymentCaption($row, $payment)
{
    return $row->paymentcaption === $payment->paymentcaption;
}

function outPutErrorReport($totalErrorCheck, $bookingErrorCheck){
//    var_export($totalErrorCheck);
//    var_export($bookingErrorCheck);
}
