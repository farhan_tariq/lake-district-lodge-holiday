<?php
include_once './dotenv.php';
(new DotEnv(__DIR__ . '/../.env'))->load();

const EXPORT_DIRECTORY = __DIR__ . '/../exports/carbis_bay/';
const IMPORT_DIRECTORY = __DIR__ . '/../imports/carbis_bay/';
const ERROR_CHECK_FILE = IMPORT_DIRECTORY . 'error_check_carbis.csv';

const FINANCE_CHECK_LIST = [1, 2, 3, 4, 5, 6, 7, 8];
const FINANCE_CHECK_THRESHOLD = 0.05;
const FINANCIAL_YEAR_START_DATE = '2020-10-03';
const GENERIC_VAT_RATE = 20;
const DECIMAL_PRECISION = 2;
const DIVISOR = 100;

$currentErrorCheck = (object)[
    'check1' => 0.0,
    'check2' => 0.0,
    'check3' => 0.0,
    'check4' => 0.0,
    'check5' => 0.0,
    'check6' => 0.0,
    'check7' => 0.0,
    'check8' => 0.0,
    'checkDate' => date('Y-m-d H:i'),
];

$totalErrorCheck = (object)[
    'check1' => 0,
    'check2' => 0,
    'check3' => 0,
    'check4' => 0,
    'check5' => 0,
    'check6' => 0,
    'check7' => 0,
    'check8' => 0,
    'checkDate' => date('Y-m-d H:i'),
];

$bookingErrorCheck = (object)[
    'check1' => [],
    'check2' => [],
    'check3' => [],
    'check4' => [],
    'check5' => [],
    'check6' => [],
    'check7' => [],
    'check8' => [],
    'checkDate' => date('Y-m-d H:i'),
];

$dodgyBookings = [
    //    Bookings failing Test 1

    //    Bookings failing Test 2

    //    Bookings failing Test 3

    //    Bookings failing Test 4

    //    Bookings failing Test 5

    //    Bookings failing Test 6

    //    Bookings failing Test 7

    //    Bookings failing Test 8

];

$financeErrorChecks = fopen(ERROR_CHECK_FILE, 'r');

if (!$financeErrorChecks) {
    die('Could not open file: "' . ERROR_CHECK_FILE . '"');
}

$headers = fgetcsv($financeErrorChecks);

$lastErrorCheck = [];
while (($row = fgetcsv($financeErrorChecks)) !== false) {
    $row = (object)array_combine($headers, $row);
    $lastErrorCheck = $row;
}

if (empty($lastErrorCheck)) {
    $lastErrorCheck = $currentErrorCheck;
}

$conn = initDB();

if (empty($argv[1])) {
    $outputFileName = 'carbis-filtered-bookings.' . date('Ymd_Hi') . '.csv';
    $out = fopen(EXPORT_DIRECTORY . $outputFileName, 'w');

    if (!$out) {
        die('Could not open file: "' . EXPORT_DIRECTORY . $outputFileName . '"');
    }
}

$newHeaders = [
    'Booking Ref',
    'Total Cost',
    'Total Rent',
    'Booking Fee',
    'Extras',
    'Charity Donation',
    'SD',
    'Due to owner RENT',
    'Due to owner EXTRAS',
    'Total Due to owner',
    'Paid to owner',
    'OWNER LIABILITY',
    'Total to Carbis',
    'Comm Gross',
    'Comm Net',
    'Comm VAT',
    'Extras Comm Gross',
    'Extras Comm Net',
    'Extras Comm VAT',
    'Total Received From Customer',
    'Total Relating to Holiday',
    'Balance Due on Holiday',
    'Total SD Due',
    'SD Amount',
    'SD Paid',
    'SD Refunded',
    'Balance of SD Held',
    'Status',
    'Property ref',
    'Owner ref',
    'Holiday booked date',
    'Holiday start date',
    'Holiday end date',
    'Source',
    'Comm%',
    'Property Name',
    'Discount',
];

if (empty($argv[1])) {
    fputcsv($out, $newHeaders);
}

$properties = getAllProperties($conn);
$bookings = getAllBookings($conn, $properties);
getAllBookingPayments($conn, $bookings);
getAllBookingExtras($conn, $bookings);
getAllCmaPayments($conn, $bookings);


foreach ($bookings as $booking) {
    $settledToOwner = getSettledToOwner($booking);
    $ownerLiability = getTotalDueToOwner($booking) - $settledToOwner;

    $commissionNet = getCommissionNet($booking);
    $commissionVAT = getCommissionVAT($booking);
    $grossCommission = $commissionNet + $commissionVAT;

    $extrasNetCommission = getExtrasNet($booking);
    $extrasNetCommissionVAT = getExtrasVAT($booking);
    $grossExtrasCommission = $extrasNetCommission + $extrasNetCommissionVAT;

    $totalReceivedFromCustomer = getTotalReceivedFromCustomer($booking);
    $totalCustomerRefunded = getAmountCustomerRefunded($booking);
    $totalRelatingToHoliday = $totalReceivedFromCustomer;
    $balanceDueOnHoliday = getBookingTotalPrice($booking) - $totalReceivedFromCustomer;

    $dueToOwnerRent = getRentalShareDueToOwner($booking);

    $sd = $booking->security_deposit_amount;
    $sdPaid = $booking->security_deposit_paid;
    $sdRefunded = getSecurityDepositRefunded($booking);

    $sdHeld = 0;
    if ($sdPaid > 0 && $sdRefunded == 0) {
        $sdHeld = $sdPaid;
    }


    $row = [
        $booking->__pk,                             // 'Booking Ref'
        getBookingTotalPrice($booking),             // 'Total Cost'
        getBookingRentalPrice($booking),            // 'Total Rent'
        $booking->booking_fee_price,                // 'Booking Fee'
        getExtrasTotal($booking),                   // 'Extras'
        getCharityDonation($booking),               // 'Charity Donation'
        $booking->security_deposit_amount,          // Security Deposit (SD)
        $dueToOwnerRent,                            // 'Due to owner RENT'
        getOwnerExtras($booking),                   // Due to owner EXTRAS: Extras Dog Example: £8
        getTotalDueToOwner($booking),               // Total Due to Owner
        $settledToOwner,                            // Paid to Owner
        $ownerLiability,                            // 'OWNER LIABILITY'
        getTotalToCarbis($booking),                 // Total to Carbis
        $grossCommission,                           // 'Comm Gross'
        $commissionNet,                             // 'Comm Net'
        $commissionVAT,                             // 'Comm VAT'
        $grossExtrasCommission,                     // 'Extras Comm Gross'
        $extrasNetCommission,                       // 'Extras Comm Net'
        $extrasNetCommissionVAT,                    // 'Extras Comm VAT'
        $totalReceivedFromCustomer,                 // 'Total Received From Customer'
        $totalRelatingToHoliday,                    // 'Total Relating to Holiday'
        $balanceDueOnHoliday,                       // 'Balance Due on Holiday'
        $booking->security_deposit_amount,          // 'Total SD Due'
        $sd,                                        // 'SD Amount'
        $sdPaid,                                    // 'SD Paid'
        $sdRefunded,                                // 'SD Refunded'
        $sdHeld,                                    // 'Balance of SD Held'
        $booking->status,                           // 'Status'
        $booking->_fk_property,                     // 'Property ref'
        !empty($booking->property) ? $booking->property->_fk_owner : ' ',              // 'Owner ref'
        $booking->booked_date,                      // 'Holiday booked date'
        $booking->from_date,                        // 'Holiday start date'
        $booking->to_date,                          // 'Holiday end date'
        $booking->source,                           // 'Source'
        getPropertyCommission($booking),            // 'Comm%'
        !empty($booking->property) ? $booking->property->name : '',                   // Property Name
        $booking->discount_price,                   // Discount
    ];

    $row = array_combine($newHeaders, $row);


    if (!empty($argv[1]) && !in_array($argv[1], FINANCE_CHECK_LIST) && $booking->__pk == $argv[1]) {
        echo 'booking_array' . PHP_EOL;
        print_r($booking);
        echo PHP_EOL . 'CSV Row (goes on the file)';
        print_r($row);
        echo PHP_EOL . '--------------------' . PHP_EOL . '--------------------' . PHP_EOL . '--------------------' . PHP_EOL;
    }

    if (!empty($argv[1]) && !in_array($booking->__pk, $dodgyBookings)) {
        // index interval: [1 - 8]
        runFinanceChecks((int)$argv[1], $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    sumFinanceChecks($row, $currentErrorCheck, $totalErrorCheck, $bookingErrorCheck);

    if (empty($argv[1])) {
        fputcsv($out, $row);
    }
}

if (empty($argv[1])) {
    fclose($out);
    echo 'Saved file: ' . $outputFileName . PHP_EOL . PHP_EOL;

    compareFinanceChecks($currentErrorCheck, $lastErrorCheck);

    // Save the new error check
    appendDataToErrorCheck(ERROR_CHECK_FILE, $currentErrorCheck);
//    outPutErrorReport($totalErrorCheck,$bookingErrorCheck);
}

function initDB()
{
    $servername = getenv('DB_SERVERNAME');
    $username = getenv('DB_USERNAME');
    $password = getenv('DB_PASSWORD');
    $database = getenv('DB_NAME');

    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $database);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    echo 'Connected' . PHP_EOL;
    return $conn;
}

function closeDB($link)
{
    mysqli_close($link);
}

function getAllBookings($conn, $properties)
{
    $sql = "SELECT * FROM booking b WHERE b.to_date > '" . FINANCIAL_YEAR_START_DATE . "';";
    $bookings = [];
    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            $bookings[$obj->__pk] = $obj;
            // TODO Get the missing properties
            $bookings[$obj->__pk]->property = !empty($properties[$obj->_fk_property]) ? $properties[$obj->_fk_property] : (var_dump($obj->_fk_property));
            $bookings[$obj->__pk]->payments = [];
            $bookings[$obj->__pk]->extras = [];
            $bookings[$obj->__pk]->cma_payments = [];
        }
    }
    echo "Returning All Bookings" . PHP_EOL;
    return $bookings;
}

function getAllProperties($conn)
{
    $sql = "SELECT * FROM property;";
    $properties = [];
    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            $properties[$obj->__pk] = $obj;
        }
    }
    echo "Returning All Properties" . PHP_EOL;
    return $properties;
}

function getAllBookingPayments($conn, $bookings)
{
    $sql = 'SELECT * FROM booking_payment';
    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (!empty($bookings[$obj->_fk_booking])) {
                $bookings[$obj->_fk_booking]->payments[] = $obj;
            }
        }
    }
    echo "Returning All Booking Payments" . PHP_EOL;
}

function getAllBookingExtras($conn, $bookings)
{
    $sql = 'SELECT * FROM booking_item';
    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (!empty($bookings[$obj->_fk_booking])) {
                $bookings[$obj->_fk_booking]->extras[] = $obj;
            }
        }
    }
    echo "Returning All Booking Extras" . PHP_EOL;
}

function getAllCmaPayments($conn, $bookings)
{
    $sql = 'SELECT * FROM cma_payments';
    if ($result = $conn->query($sql)) {
        while ($obj = $result->fetch_object()) {
            if (!empty($bookings[$obj->_fk_booking])) {
                $bookings[$obj->_fk_booking]->cma_payments[] = $obj;
            }
        }
    }
    echo "Returning All CMA Payments" . PHP_EOL;
}

function getBookingTotalPrice($booking)
{
    // Booking 1011 has discount without voucher and already applied to total cost
    // Booking 10587 has discount already applied to total cost
    // Booking 11633 has rent higher than the total cost
    $originalTotalPrice = (float)$booking->total_price;
    $rent = getBookingRentalPrice($booking);
    $extras = getExtrasTotal($booking);
    $bookingFee = $booking->booking_fee_price;
    $charityDonation = getCharityDonation($booking);

    $total = $rent + $extras + $bookingFee + $charityDonation;

    if (roundFloat($originalTotalPrice) > roundFloat($total)) {
        return $originalTotalPrice - $booking->discount_price;
    }

    return $originalTotalPrice;
}

function getBookingRentalPrice($booking)
{
    return $booking->rental_price - $booking->discount_price;
}

function getExtrasTotal($booking)
{
    $total = 0;
    foreach ($booking->extras as $extra) {
        if (stripos($extra->extra_type, 'donation') === false) {
            $total += $extra->total_price;
        }
    }
    return $total;
}

function getCharityDonation($booking)
{
    $total = 0;
    foreach ($booking->extras as $extra) {
        if (stripos($extra->extra_type, 'donation') !== false) {
            $total += $extra->price * $extra->quantity;
        }
    }

    return $total;
}

function getOwnerExtras($booking)
{
    $total = 0;
    foreach ($booking->extras as $extra) {
        if ($extra->commissionable == 0) {
            $total += $extra->price * $extra->quantity;
        }
    }

    return $total;
}

function getRentalShareDueToOwner($booking)
{
    $rent = getBookingRentalPrice($booking);
    $carbisCommission = getBookingCommission($booking);
    $ownerExtrasNetCommission = getExtrasNet($booking);
    $ownerTaxDeductionCharge = ($carbisCommission + $ownerExtrasNetCommission) * (GENERIC_VAT_RATE / 100);

    return $rent - $carbisCommission - $ownerTaxDeductionCharge;
}

function getSettledToOwner($booking)
{
    $total = 0;
    foreach ($booking->payments as $payment) {
        if ($payment->payment_to == 'Supplier' && $payment->status == 'Paid') {
            $total += $payment->amount;
        }
    }
    return $total;
}

function getTotalToCarbis($booking)
{
    return getCommissionNet($booking) + getCommissionVAT($booking) + $booking->booking_fee_price;
}

function getCommissionNet($booking)
{
    $total = 0;
    foreach ($booking->extras as $extra) {
        if ($extra->commissionable == 1) {
            $total += $extra->total_price;
        }
    }

    return $total + getBookingCommission($booking);
}

function getCommissionVAT($booking)
{
    $vat = 0;
    foreach ($booking->extras as $extra) {
        if ($extra->commissionable == 1) {
            $vat += $extra->total_price;
        }
    }
    $bookingCommission = getBookingCommission($booking);

    return ($vat + $bookingCommission) * (GENERIC_VAT_RATE / DIVISOR);
}

function getExtrasNet($booking)
{
    $total = 0;

    foreach ($booking->extras as $extra) {
        if ($extra->commissionable == 0) {
            $total += $extra->total_price - ($extra->price * $extra->quantity);
        }
    }

    return $total;
}

function getExtrasVAT($booking)
{
    $vat = 0;

    foreach ($booking->extras as $extra) {
        if ($extra->commissionable == 0) {
            $vat += $extra->total_price - ($extra->price * $extra->quantity);
        }
    }

    return $vat * (GENERIC_VAT_RATE / DIVISOR);
}

function getTotalReceivedFromCustomer($booking)
{
    $total = 0;
    foreach ($booking->payments as $payment) {
        if ($payment->type == 'Receipt from Renter' && $payment->status == 'Paid') {
            $total += $payment->amount;
        }
    }

    return $total;
}

function getAmountCustomerRefunded($booking)
{
    // TODO get Amount Customer Refunded
}

function getSecurityDepositRefunded($booking)
{
    $securityDeposits = [];
    foreach ($booking->payments as $payment) {
        if ($payment->type == 'Security Deposit' && $payment->category == 'Balance' && $payment->payment_to == 'Client' && $payment->status == 'Paid') {
            $securityDeposits[] = $payment->amount;
        }
    }

    if (count($securityDeposits) > 1) {
        return end($securityDeposits);
    }

    return 0;
}

function getTotalDueToOwner($booking)
{
    $rent = getBookingRentalPrice($booking);
    $commission = getBookingCommission($booking);
    $totalOwnerExtras = getOwnerExtras($booking);
    $ownerExtrasNetCommission = getExtrasNet($booking);
    $ownerTaxDeductionCharge = ($commission + $ownerExtrasNetCommission) * (GENERIC_VAT_RATE / 100);

    return $rent + $totalOwnerExtras - $commission - $ownerTaxDeductionCharge;
}

function getPropertyCommission($booking)
{
    if (!empty($booking->property->commission_percentage)) {
        return $booking->property->commission_percentage;
    }

    return 0;
}

function getBookingCommission($booking)
{
    return getBookingRentalPrice($booking) * (getPropertyCommission($booking) / DIVISOR);
}

function getCancellationFee($booking)
{
    $total = 0;
    if (isBookingCancelled($booking)) {
        foreach ($booking->extras as $extra) {
            if ($extra->commissionable == 1 && stripos($extra->extra_type, 'cancellation') !== false) {
                $total += $extra->total_price;
            }
        }
        return $total;
    }

    return 0;
}

function isBookingCancelled($booking)
{
    return $booking->status === 'Cancelled';
}

function roundFloat($num)
{
    return round($num, DECIMAL_PRECISION);
}

function getDataByField($array, $id, $field)
{
    return $array[$id]->{$field};
}

/**
 * Possible Algorithm
 * - $properties = [
 *      propertyID
 *
 * ];
 * - $bookings = [
 *      __pk
 *      _fk_property
 *      _fk_customer
 *      booked_date
 *      from_date
 *      to_date
 *      status (Active, Cancelled)
 *      adults
 *      children
 *      infants
 *      pets (not used - this is in the booking_item table)
 *      source (owner booking, google, previous guest...)
 *      owner_booking_type (not used)
 *      total_price
 *      rental_price
 *      booking_fee_price
 *      discount_price
 *      credit_card_fee_amount (not used)
 *      extras_price (not used)
 *      charity_deposit_price (not used)
 *      extras_commission (not used)
 *      commission_rate (not used)
 *      due_to_owner          => Can make a check to see if all Adds up
 *      paid_to_owner
 *      outstanding_to_owner
 *      deposit_amount
 *      deposit_due_date
 *      balance_amount
 *      balance_due_date
 *      security_deposit_amount
 *      security_deposit_paid
 *      security_deposit_due_date
 *      outstanding_balance
 * ];
 * - $bookings[]->items = [ // Extras
 *      __pk
 *      _fk_booking
 *      extra_type
 *      quantity
 *      description (not used)
 *      price
 *      total_price (quantity * price)
 *      compulsory (always 1)
 *      commissionable (0 or 1)
 *          When 1: the extras amount is paid to Carbis
 *          When 0: the extras amount is paid to Owner
 *      vatable (always 0)
 *      commission_percent (always 100% when commissionable is 1)
 *          (always 0% when commissionable is 0)
 *      cancelled (always 0)
 * ]
 * - $bookings[]->payments = [ // Finances
 *      __pk
 *      _fk_booking
 *      type (Receipt from Renter, Security Deposit, Payment to Owner, Refund to Guest)
 *      category (Deposit, Booking Fee, Balance, Holiday Extra, Part Payment, Damages Payment, Compensation Awarded)
 *      name (186 different names)
 *      payment_to (Client, Supplier)
 *      payment_method (13 different payment_methods + NULL)
 *          Most of the NULL values are for "Damages Payment"
 *      payment_date
 *      payment_due_date
 *      payment_paid_time
 *      amount
 *      status (Paid, Overdue, Underpaid, Pending, Overpaid)
 *      comments
 * ]
 * - $bookings[]->change_of_dates = [ // cma_booking_change_of_dates
 *      _fk_booking
 *      was_start_date
 *      was_end_date
 *      now_start_date
 *      now_end_date
 *      was_price
 *      now_price
 *      date_actioned
 * ]
 */


function financeCheck($checkSum)
{
    return abs($checkSum) < FINANCE_CHECK_THRESHOLD;
}

function runFinanceChecks($index, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    // Total Cost - Rent +Extras + Bk Fee + Charity Donation
    if ($index === 1) { // =B3-C3-D3-E3-F3
        $financeCheck = ($row['Total Cost'] - $row['Total Rent'] - $row['Booking Fee'] - $row['Extras'] - $row['Charity Donation']);
        financeCheckOne($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    // Total Cost - Total To owner + Total to LDLH
    if ($index === 2) { // =B3-J3-M3
        $financeCheck = ($row['Total Cost'] - $row['Total Due to owner'] - $row['Total to Carbis'] - $row['Charity Donation']);
        financeCheckTwo($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    // Owner Total - Rent + Extras
    if ($index === 3) { // =J3-I3-H3
        $financeCheck = ($row['Total Due to owner'] - $row['Due to owner EXTRAS'] - $row['Due to owner RENT']);
        financeCheckThree($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    // LDLH Total - Comm Rent + Comm Extras + Bk Fee
    if ($index === 4) { // =M3-N3-Q3-T3
        $financeCheck = ($row['Total to Carbis'] - $row['Comm Gross'] - $row['Booking Fee']);
        financeCheckFour($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    // Rent - Owner Rent + Comm Rent
    if ($index === 5) { // =C3-H3-N3
        $financeCheck = ($row['Total Rent'] - $row['Due to owner RENT'] - $row['Comm Gross']);
        financeCheckFive($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    // Total Extras - Owner Extras - LDLH Extras
    if ($index === 6) { // =E3-I3-Q3
        $financeCheck = ($row['Extras'] - $row['Due to owner EXTRAS'] - $row['Extras Comm Gross']);
        financeCheckSix($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    // Total to Owner - Total Paid -Total Due
    if ($index === 7) { // =J3-K3-L3
        $financeCheck = ($row['Total Due to owner'] - $row['Paid to owner'] - $row['OWNER LIABILITY']);
        financeCheckSeven($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }

    // Total Cost - Received from Customer - Balance due
    if ($index === 8) { // =B3-V3-X3
        $financeCheck = ($row['Total Cost'] - $row['Total Received From Customer'] - $row['Balance Due on Holiday']);
        financeCheckEight($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded);
    }
}

function financeCheckOne($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    // skip when difference is the same amount as the voucher

    if (!financeCheck($financeCheck)) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 01 failed: ' => $booking->__pk,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '($row["Total Cost"] - $row["Total Rent"] - $row["Booking Fee"] - $row["Extras"] - $row["Charity Donation"])',
                'Total Cost' => $row['Total Cost'],
                'Total Rent' => (float)$row['Total Rent'],
                'Booking Fee' => $row['Booking Fee'],
                'Extras' => $row['Extras'],
                'Charity Donation' => $row['Charity Donation'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function financeCheckTwo($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    if (!financeCheck($financeCheck)) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 02 failed: ' => $booking->__pk,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '$row["Total Cost"] - $row["Total Due to owner"] - $row["Total to Carbis"] - $row["Charity Donation"]',
                'Total Cost' => $row['Total Cost'],
                'Total Due to owner' => $row['Total Due to owner'],
                'Total to Carbis' => $row['Total to Carbis'],
                'Charity Donation' => $row['Charity Donation'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function financeCheckThree($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    if (!financeCheck($financeCheck)) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 03 failed: ' => $booking->__pk,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '$row["Total Due to owner"] - $row["Due to owner EXTRAS"] - $row["Due to owner RENT"]',
                'Total Due to owner' => $row['Total Due to owner'],
                'Due to owner EXTRAS' => $row['Due to owner EXTRAS'],
                'Due to owner RENT' => $row['Due to owner RENT'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function financeCheckFour($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    if (!financeCheck($financeCheck)) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 04 failed: ' => $booking->__pk,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '$row["Total to Carbis"] - $row["Comm Gross"] - $row["Booking Fee"]',
                'Total to LDLH' => $row['Total to Carbis'],
                'Comm Gross' => $row['Comm Gross'],
                'Booking Fee' => $row['Booking Fee'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function financeCheckFive($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    if (!financeCheck($financeCheck)) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 05 failed: ' => $booking->__pk,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '$row["Total Rent"] - $row["Due to owner RENT"] - $row["Comm Gross"]',
                'Total Rent' => $row['Total Rent'],
                'Due to owner RENT' => $row['Due to owner RENT'],
                'Comm Gross' => $row['Comm Gross'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function financeCheckSix($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    if (!financeCheck($financeCheck)) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 06 failed: ' => $booking->__pk,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '$row["Extras"] - $row["Due to owner EXTRAS"]',
                'Extras' => $row['Extras'],
                'Due to owner EXTRAS' => $row['Due to owner EXTRAS'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function financeCheckSeven($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    if (!financeCheck($financeCheck)) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 07 failed: ' => $booking->__pk,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '$row["Total Due to owner"] - $row["Paid to owner"] - $row["OWNER LIABILITY"]',
                'Total Due to owner' => $row['Total Due to owner'],
                'Paid to owner' => $row['Paid to owner'],
                'OWNER LIABILITY' => $row['OWNER LIABILITY'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function financeCheckEight($financeCheck, $booking, $row, $sdPaid, $sdHeld, $sdRefunded)
{
    if (!financeCheck($financeCheck)) {
        print_r($row);
        print_r($booking);

        var_export(
            [
                'Check 08 failed: ' => $booking->__pk,
                'sdPaid' => $sdPaid,
                'sdHeld' => $sdHeld,
                'sdRefunded' => $sdRefunded,
                'financeCheck' => $financeCheck,
                'check FORMULA' => '$row["Total Cost"] - $row["Total Received From Customer"] - $row["Balance Due on Holiday"]',
                'Total Cost' => $row['Total Cost'],
                'Total Received From Customer' => $row['Total Received From Customer'],
                'Balance Due on Holiday' => $row['Balance Due on Holiday'],

                'Booking Status' => $row['Status']
            ]
        );
        die;
    }
}

function appendDataToErrorCheck($fileName, $fileData)
{
    $fileStream = fopen($fileName, 'a');

    if (!$fileStream) {
        echo PHP_EOL . 'ERROR - Could not open file "' . $fileName . '"' . PHP_EOL;
        return;
    }

    // Write data
    fputcsv($fileStream, get_object_vars($fileData));

    fclose($fileStream);
}

function sumFinanceChecks($row, $currentErrorCheck, $totalErrorCheck, $bookingErrorCheck)
{
    $financeCheck1 = ($row['Total Cost'] - $row['Total Rent'] - $row['Booking Fee'] - $row['Extras'] - $row['Charity Donation']);
    $financeCheck2 = ($row['Total Cost'] - $row['Total Due to owner'] - $row['Total to Carbis'] - $row['Charity Donation']);
    $financeCheck3 = ($row['Total Due to owner'] - $row['Due to owner EXTRAS'] - $row['Due to owner RENT']);
    $financeCheck4 = ($row['Total to Carbis'] - $row['Comm Gross'] - $row['Booking Fee']);
    $financeCheck5 = ($row['Total Rent'] - $row['Due to owner RENT'] - $row['Comm Gross']);
    $financeCheck6 = ($row['Extras'] - $row['Due to owner EXTRAS']);
    $financeCheck7 = ($row['Total Due to owner'] - $row['Paid to owner'] - $row['OWNER LIABILITY']);
    $financeCheck8 = ($row['Total Cost'] - $row['Total Received From Customer'] - $row['Balance Due on Holiday']);

    $totalErrorCheck->check1 += financeCheck($financeCheck1) ? 0 : 1;
    $totalErrorCheck->check2 += financeCheck($financeCheck2) ? 0 : 1;
    $totalErrorCheck->check3 += financeCheck($financeCheck3) ? 0 : 1;
    $totalErrorCheck->check4 += financeCheck($financeCheck4) ? 0 : 1;
    $totalErrorCheck->check5 += financeCheck($financeCheck5) ? 0 : 1;
    $totalErrorCheck->check6 += financeCheck($financeCheck6) ? 0 : 1;
    $totalErrorCheck->check7 += financeCheck($financeCheck7) ? 0 : 1;
    $totalErrorCheck->check8 += financeCheck($financeCheck8) ? 0 : 1;


    if (!financeCheck($financeCheck1)) $bookingErrorCheck->check1[] = $row['Booking Ref'];
    if (!financeCheck($financeCheck2)) $bookingErrorCheck->check2[] = $row['Booking Ref'];
    if (!financeCheck($financeCheck3)) $bookingErrorCheck->check3[] = $row['Booking Ref'];
    if (!financeCheck($financeCheck4)) $bookingErrorCheck->check4[] = $row['Booking Ref'];
    if (!financeCheck($financeCheck5)) $bookingErrorCheck->check5[] = $row['Booking Ref'];
    if (!financeCheck($financeCheck6)) $bookingErrorCheck->check6[] = $row['Booking Ref'];
    if (!financeCheck($financeCheck7)) $bookingErrorCheck->check7[] = $row['Booking Ref'];
    if (!financeCheck($financeCheck8)) $bookingErrorCheck->check8[] = $row['Booking Ref'];

    $currentErrorCheck->check1 += abs($financeCheck1);
    $currentErrorCheck->check2 += abs($financeCheck2);
    $currentErrorCheck->check3 += abs($financeCheck3);
    $currentErrorCheck->check4 += abs($financeCheck4);
    $currentErrorCheck->check5 += abs($financeCheck5);
    $currentErrorCheck->check6 += abs($financeCheck6);
    $currentErrorCheck->check7 += abs($financeCheck7);
    $currentErrorCheck->check8 += abs($financeCheck8);
}

function compareFinanceChecks($currentErrorCheck, $lastErrorCheck)
{
    echo 'Error check comparison:' . PHP_EOL;

    $differenceCheck1 = round(abs($currentErrorCheck->check1) - $lastErrorCheck->check1, 2);
    $differenceCheck2 = round(abs($currentErrorCheck->check2) - $lastErrorCheck->check2, 2);
    $differenceCheck3 = round(abs($currentErrorCheck->check3) - $lastErrorCheck->check3, 2);
    $differenceCheck4 = round(abs($currentErrorCheck->check4) - $lastErrorCheck->check4, 2);
    $differenceCheck5 = round(abs($currentErrorCheck->check5) - $lastErrorCheck->check5, 2);
    $differenceCheck6 = round(abs($currentErrorCheck->check6) - $lastErrorCheck->check6, 2);
    $differenceCheck7 = round(abs($currentErrorCheck->check7) - $lastErrorCheck->check7, 2);
    $differenceCheck8 = round(abs($currentErrorCheck->check8) - $lastErrorCheck->check8, 2);

    var_export([
        'Last error check' => [
            'check1' => financeCheck($lastErrorCheck->check1) ? 'pass' : $lastErrorCheck->check1,
            'check2' => financeCheck($lastErrorCheck->check2) ? 'pass' : $lastErrorCheck->check2,
            'check3' => financeCheck($lastErrorCheck->check3) ? 'pass' : $lastErrorCheck->check3,
            'check4' => financeCheck($lastErrorCheck->check4) ? 'pass' : $lastErrorCheck->check4,
            'check5' => financeCheck($lastErrorCheck->check5) ? 'pass' : $lastErrorCheck->check5,
            'check6' => financeCheck($lastErrorCheck->check6) ? 'pass' : $lastErrorCheck->check6,
            'check7' => financeCheck($lastErrorCheck->check7) ? 'pass' : $lastErrorCheck->check7,
            'check8' => financeCheck($lastErrorCheck->check8) ? 'pass' : $lastErrorCheck->check8,
            'checkDate' => $lastErrorCheck->checkDate,
        ],
        'Current error check' => [
            'check1' => financeCheck($currentErrorCheck->check1) ? 'pass' : round($currentErrorCheck->check1, 2),
            'check2' => financeCheck($currentErrorCheck->check2) ? 'pass' : round($currentErrorCheck->check2, 2),
            'check3' => financeCheck($currentErrorCheck->check3) ? 'pass' : round($currentErrorCheck->check3, 2),
            'check4' => financeCheck($currentErrorCheck->check4) ? 'pass' : round($currentErrorCheck->check4, 2),
            'check5' => financeCheck($currentErrorCheck->check5) ? 'pass' : round($currentErrorCheck->check5, 2),
            'check6' => financeCheck($currentErrorCheck->check6) ? 'pass' : round($currentErrorCheck->check6, 2),
            'check7' => financeCheck($currentErrorCheck->check7) ? 'pass' : round($currentErrorCheck->check7, 2),
            'check8' => financeCheck($currentErrorCheck->check8) ? 'pass' : round($currentErrorCheck->check8, 2),
            'checkDate' => $currentErrorCheck->checkDate,
        ],
        'Difference' => [
            'check1' => financeCheck($differenceCheck1) ? 'no difference' : $differenceCheck1,
            'check2' => financeCheck($differenceCheck2) ? 'no difference' : $differenceCheck2,
            'check3' => financeCheck($differenceCheck3) ? 'no difference' : $differenceCheck3,
            'check4' => financeCheck($differenceCheck4) ? 'no difference' : $differenceCheck4,
            'check5' => financeCheck($differenceCheck5) ? 'no difference' : $differenceCheck5,
            'check6' => financeCheck($differenceCheck6) ? 'no difference' : $differenceCheck6,
            'check7' => financeCheck($differenceCheck7) ? 'no difference' : $differenceCheck7,
            'check8' => financeCheck($differenceCheck8) ? 'no difference' : $differenceCheck8,
        ]
    ]);
}