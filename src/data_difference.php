<?php

const EXPORT_DIRECTORY = __DIR__ . '/../exports/';
const EXPORT_28TH_FEB_DATA = 'filtered-bookings.20210406_1609 ( 28th data - Fix for Airbnb along with other Fixes).csv';
const EXPORT_22ND_MARCH_DATA = 'filtered-bookings.20210406_2319(22nd March Data).csv';


$filteredBookingFebData = fopen(EXPORT_DIRECTORY . EXPORT_28TH_FEB_DATA, 'r');

if (!$filteredBookingFebData) {
    die('Could not open file: "' . EXPORT_DIRECTORY . EXPORT_28TH_FEB_DATA . '"');
}

$headers = fgetcsv($filteredBookingFebData);

$febData = [];
while (($row = fgetcsv($filteredBookingFebData)) !== false) {
    $row = (object)array_combine($headers, $row);
    $bookingRef = 'Booking Ref';
    $febData[$row->$bookingRef] = $row;
}

$filteredBookingMarchData = fopen(EXPORT_DIRECTORY . EXPORT_22ND_MARCH_DATA, 'r');

if (!$filteredBookingMarchData) {
    die('Could not open file: "' . EXPORT_DIRECTORY . EXPORT_22ND_MARCH_DATA . '"');
}

$headers = fgetcsv($filteredBookingMarchData);


$marchData = [];
while (($row = fgetcsv($filteredBookingMarchData)) !== false) {
    $row = (object)array_combine($headers, $row);
    $bookingRef = 'Booking Ref';
    $marchData[$row->$bookingRef] = $row;
}


$ignoreHeaders = [
    'Booking Ref',
    'Source',
    'Cottage Name',
    'Cottage Short Name',
    'Payments To More than 1 Owner',
    'holidayBookedDate' => 'Holiday booked date',
    'holidayStartDate' => 'Holiday start date',
    'holidayEndDate' => 'Holiday end date',
    'status' => 'Status',
    'propertyRef' => 'PropertyRef',
];

$intHeaders =[
    'propertyRef' => 'Property ref',
    'ownerRef' => 'Owner ref',
    'multiBookingRef' => 'Multi Booking Ref',
];

$floatHeaders =[
    'totalCost' => 'Total Cost',
    'totalRent' => 'Total Rent',
    'bookingFee' => 'Booking Fee',
    'extras' => 'Extras',
    'charityDonation' => 'Charity Donation',
    'sd' => 'SD',
    'dueToOwnerRENT' => 'Due to owner RENT',
    'dueToOwnerEXTRAS' => 'Due to owner EXTRAS',
    'totalDueToOwner' => 'Total Due to owner',
    'paidToOwner' => 'Paid to owner',
    'ownerLiability' => 'OWNER LIABILITY',
    'totalToLdlh' => 'Total to LDLH',
    'commGross' => 'Comm Gross',
    'commNet' => 'Comm Net',
    'commVat' => 'Comm VAT',
    'extraCommGross' => 'Extras Comm Gross',
    'extraCommNet' => 'Extras Comm Net',
    'extraCommVat' => 'Extras Comm VAT',
    'bookingFee2' => 'Booking Fee 2',
    'totalReceivedFromCustomer' => 'Total Received From Customer',
    'totalRelatingToHoliday' => 'Total Relating to Holiday',
    'balanceDueOnHoliday' => 'Balance Due on Holiday',
    'totalSDDue' => 'Total SD Due',
    'sdPaid' => 'SD Paid',
    'sdRefunded' => 'SD Refunded',
    'balanceOfSDHeld' => 'Balance of SD Held',
    'discount' => 'Discount',
    'bookingProtectionCharge' => 'Booking Protection Charge',
    'otherCharges' => 'Other Charges',
    'voucher' => 'Voucher',
    'adminCharge' => 'Admin Charge',
];

$stringHeaders =[
    'Source',
    'Cottage Name',
    'Cottage Short Name',
    'bookingType' => 'Booking Type',
    'Payments To More than 1 Owner',
    'status' => 'Status',
    'comm%' => 'Comm%',
];

$dateHeaders = [
    'holidayBookedDate' => 'Holiday booked date',
    'holidayStartDate' => 'Holiday start date',
    'holidayEndDate' => 'Holiday end date',
];

$newHeaders = [
    'bookingRef' => 'Booking Ref',
    'totalCost' => 'Total Cost',
    'totalRent' => 'Total Rent',
    'bookingFee' => 'Booking Fee',
    'extras' => 'Extras',
    'charityDonation' => 'Charity Donation',
    'sd' => 'SD',
    'dueToOwnerRENT' => 'Due to owner RENT',
    'dueToOwnerEXTRAS' => 'Due to owner EXTRAS',
    'totalDueToOwner' => 'Total Due to owner',
    'paidToOwner' => 'Paid to owner',
    'ownerLiability' => 'OWNER LIABILITY',
    'totalToLdlh' => 'Total to LDLH',
    'commGross' => 'Comm Gross',
    'commNet' => 'Comm Net',
    'commVat' => 'Comm VAT',
    'extraCommGross' => 'Extras Comm Gross',
    'extraCommNet' => 'Extras Comm Net',
    'extraCommVat' => 'Extras Comm VAT',
    'bookingFee2' => 'Booking Fee 2',
    'totalReceivedFromCustomer' => 'Total Received From Customer',
    'totalRelatingToHoliday' => 'Total Relating to Holiday',
    'balanceDueOnHoliday' => 'Balance Due on Holiday',
    'totalSDDue' => 'Total SD Due',
    'sdPaid' => 'SD Paid',
    'sdRefunded' => 'SD Refunded',
    'balanceOfSDHeld' => 'Balance of SD Held',
    'status' => 'Status',
    'propertyRef' => 'Property ref',
    'ownerRef' => 'Owner ref',
    'holidayBookedDate' => 'Holiday booked date',
    'holidayStartDate' => 'Holiday start date',
    'holidayEndDate' => 'Holiday end date',
    'source' => 'Source',
    'comm%' => 'Comm%',
    'cottageName' => 'Cottage Name',
    'cottageShortName' => 'Cottage Short Name',
    'bookingType' => 'Booking Type',
    'discount' => 'Discount',
    'bookingProtectionCharge' => 'Booking Protection Charge',
    'multiBookingRef' => 'Multi Booking Ref',
    'otherCharges' => 'Other Charges',
    'voucher' => 'Voucher',
    'paymentToMoreThan1Owner' => 'Payments To More than 1 Owner',
    'adminCharge' => 'Admin Charge',
];



$outputData =[];

if (empty($argv[1])) {
    $outputFileName = 'diff-filtered-bookings.' . date('Ymd_Hi') . '.csv';
    $out = fopen(EXPORT_DIRECTORY . $outputFileName, 'w');

    if (!$out) {
        die('Could not open file: "' . EXPORT_DIRECTORY . $outputFileName . '"');
    }
}

if (empty($argv[1])) {
    fputcsv($out, $newHeaders);
}

foreach ($febData as $febDatum) {
    $bookingRef = 'Booking Ref';
    $differences = [];
    if (!empty($marchData[$febDatum->$bookingRef])) {
        $differences[$bookingRef] = $febDatum->$bookingRef;

        foreach ($newHeaders as $key => $header) {
            $isSame =$febDatum->$header === $marchData[$febDatum->$bookingRef]->$header ;
            if (in_array($header, $intHeaders)){
                $differences[$header] = $isSame ? $marchData[$febDatum->$bookingRef]->$header : (int)$febDatum->$header - (int)$marchData[$febDatum->$bookingRef]->$header;
            }

            if (in_array($header, $floatHeaders)){
                $differences[$header] = !$isSame ? $febDatum->$header * 1 - $marchData[$febDatum->$bookingRef]->$header * 1 : '';
            }

            if (in_array($header, $stringHeaders)){
                $differences[$header] = !$isSame ? $febDatum->$header . '-' . $marchData[$febDatum->$bookingRef]->$header : '';
            }

            if (in_array($header, $dateHeaders)) {
                $dStart = new DateTime($febDatum->$header);
                $dEnd = new DateTime($marchData[$febDatum->$bookingRef]->$header);
                $dDiff = $dStart->diff($dEnd);
                $date1 = date('Y-m-d', strtotime($febDatum->$header));
                $date2 = date('Y-m-d', strtotime($marchData[$febDatum->$bookingRef]->$header));

                $differences[$header] = !$isSame ? $dDiff->days : '';
            }
        }

        $differences = array_combine($newHeaders, $differences);
        fputcsv($out, $differences);
    }
}

fclose($out);
echo 'Saved file: ' . $outputFileName . PHP_EOL . PHP_EOL;
