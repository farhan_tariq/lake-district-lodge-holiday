<?php

const IMPORT_DIRECTORY = __DIR__ . '/../imports/';
const ACTUAL_FILE = 'actual_values.csv';
const DEBTORS_FILE = 'debtors_report.csv';


$actualValues = [];
$actual = fopen(IMPORT_DIRECTORY . ACTUAL_FILE, 'r');

if (!$actual) {
    die('Could not open file: "' . IMPORT_DIRECTORY . ACTUAL_FILE . '"');
}

$headers = fgetcsv($actual);


while (($row = fgetcsv($actual)) !== false) {
    $row = (object)array_combine($headers, $row);
    $actualValues[] = $row;
}


$debtorsValues = [];
$debtors = fopen(IMPORT_DIRECTORY . DEBTORS_FILE, 'r');

if (!$debtors) {
    die('Could not open file: "' . IMPORT_DIRECTORY . DEBTORS_FILE . '"');
}

$headers = fgetcsv($debtors);

while (($row = fgetcsv($debtors)) !== false) {
    $row = (object)array_combine($headers, $row);
    $debtorsValues[] = $row;
}


$bookings = [];
$structure = (object)[
    'booking_ref' => 0,
    'expected' => 0,
    'actual' => 0,
    'difference' => 0
];

$outputFileName = 'debtors_report.' . date('Ymd_Hi') . '.csv';
$out = fopen(IMPORT_DIRECTORY . $outputFileName, 'w');

if (!$out) {
    die('Could not open file: "' . IMPORT_DIRECTORY . $outputFileName . '"');
}

$newHeaders = [
    'booking_ref',
    'expected',
    'actual',
    'difference',
];

fputcsv($out, $newHeaders);

foreach ($debtorsValues as $key => $debtorValue) {
    $data = $structure;
    foreach ($actualValues as $actualValue) {
        if (
            $actualValue->booking_ref === $debtorValue->booking_id ||
            strpos($actualValue->booking_ref, $debtorValue->booking_id) !== false
        ) {
            if (!empty($bookings[$debtorValue->booking_id]->data)) {
                $bookings[$debtorValue->booking_id]->data->booking_ref = $actualValue->booking_ref;
                $bookings[$debtorValue->booking_id]->data->expected = $debtorValue->value;
                $bookings[$debtorValue->booking_id]->data->actual = $actualValue->debtors;
                $bookings[$debtorValue->booking_id]->data->difference = $data->expected - $data->actual;
            } else {
                $data->booking_ref = $actualValue->booking_ref;
                $data->expected = $debtorValue->value;
                $data->actual = $actualValue->debtors;
                $data->difference = $data->expected - $data->actual;
                $bookings[$debtorValue->booking_id]= (object)[];
                $bookings[$debtorValue->booking_id]->data= $data;
            }
        }
    }

    if (!empty($bookings[$debtorValue->booking_id])){
        if ($bookings[$debtorValue->booking_id]->data->difference != 0){
            fputcsv($out, (array)$bookings[$debtorValue->booking_id]->data);
        }
    }
}



fclose($out);



