<?php
include_once './dotenv.php';
(new DotEnv(__DIR__ . '/../.env'))->load();

const EXPORT_DIRECTORY = __DIR__ . '/../exports/';
const FILENAME = 'jons-expected.20210414';
const FILTERED_BOOKINGS = 'filtered-bookings.' . FILENAME . '.csv';

$conn = initDB();

$bookingCount = 'Multi Booking Count';
$debtorsControlAccount = 'Debtors Control Account';
$sykesDeferred = 'Sykes Deferred';
$ownerDeferred = 'Owner Deferred';
$ownerBalances = 'Owner Balances';
$bankCurrentAccount = 'Bank Current Account';
$sykesRecIncome = 'Sykes Rec Income';


$filteredBookings = fopen(EXPORT_DIRECTORY . FILTERED_BOOKINGS, 'r');

if (!$filteredBookings) {
    die('Could not open file: "' . EXPORT_DIRECTORY . FILTERED_BOOKINGS . '"');
}

$headers = fgetcsv($filteredBookings);
$sql = "INSERT INTO expected_values 
    (`booking_ref`, `debtors`, `bank`, `sykes_rec_income`, `sykes_def`, `owner_def`, `owner_balances`) VALUES ";

while (($row = fgetcsv($filteredBookings)) !== false) {
    $row = (object)array_combine($headers, $row);

    // SQL to use with Jon SpreadSheet data
    $sql .= "('{$row->$bookingCount}',
     {$row->$debtorsControlAccount},
     {$row->$bankCurrentAccount},
     {$row->$sykesRecIncome},
     {$row->$sykesDeferred},
     {$row->$ownerDeferred},
     {$row->$ownerBalances}),";

    // SQL from jon.php script
//    $sql .= "('{$row->$bookingCount}',
//     {$row->$debtorsControlAccount},
//     {$row->$bankCurrentAccount},
//     -{$row->$sykesRecIncome},
//     -{$row->$sykesDeferred},
//     -{$row->$ownerDeferred},
//     -{$row->$ownerBalances}),";
}

$sql = substr($sql,0, -1);

$conn->query('TRUNCATE TABLE expected_values;');
$conn->query($sql);

$conn->close();


function initDB()
{
    $servername = getenv('DB_OPTEST_SERVERNAME');
    $username = getenv('DB_OPTEST_USERNAME');
    $password = getenv('DB_OPTEST_PASSWORD');
    $database = getenv('DB_OPTEST_NAME');

    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $database);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    echo 'Connected' . PHP_EOL;
    return $conn;
}