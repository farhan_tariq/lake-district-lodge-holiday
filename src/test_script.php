<?php
const IMPORT_DIRECTORY = __DIR__ .'/../imports/';
const EXPORT_DIRECTORY = __DIR__ .'/../exports/';
const CHECK_FILE = 'filtered-bookings.20210319_1214.csv';
const BOOKING_DETAILS_FILE = '16-03-2021/2359-booking_details.csv';

//filteredBookingsFile();
//bookingDetailsFile();
//test_checkBookingPaymentsToMoreThanOneOwner();
test_str_replace();

function filteredBookingsFile()
{
    $filteredBookings = fopen(EXPORT_DIRECTORY . CHECK_FILE, 'r');

    if (!$filteredBookings) {
        die('Could not open file: "' . EXPORT_DIRECTORY . CHECK_FILE . '"');
    }

    $headers = fgetcsv($filteredBookings);

    $data = [];
    while (($row = fgetcsv($filteredBookings)) !== false) {
        $row = (object)array_combine($headers, $row);

        if (empty($data[$row->{'Booking Ref'}])) {
            $data[$row->{'Booking Ref'}][] = (object)[
                'MultiBookingRef' => $row->{'Multi Booking Ref'},
                'Status' => $row->{'Booking Status'},
            ];
            $data[$row->{'Booking Ref'}]['statistics'] = (object)[
                'ConfirmedStatusCount' => 0,
                'CanceledStatusCount' => 0,
            ];

            if (stripos($row->{'Booking Status'}, 'confirmed') !== false) {
                $data[$row->{'Booking Ref'}]['statistics']->ConfirmedStatusCount++;
            }
            if (stripos($row->{'Booking Status'}, 'canceled') !== false) {
                $data[$row->{'Booking Ref'}]['statistics']->CanceledStatusCount++;
            }
        } else {
            $data[$row->{'Booking Ref'}][] = (object)[
                'MultiBookingRef' => $row->{'Multi Booking Ref'},
                'Status' => $row->{'Booking Status'},
            ];
            if (stripos($row->{'Booking Status'}, 'confirmed') !== false) {
                $data[$row->{'Booking Ref'}]['statistics']->ConfirmedStatusCount++;
            }
            if (stripos($row->{'Booking Status'}, 'canceled') !== false) {
                $data[$row->{'Booking Ref'}]['statistics']->CanceledStatusCount++;
            }
        }
    }

    fclose($filteredBookings);

    foreach ($data as $index => $d) {
        if ($d['statistics']->ConfirmedStatusCount > 1) {
            print_r([$index, $d]);
        }
    }
//print_r($data);
}


function bookingDetailsFile()
{
    $filteredBookings = fopen(IMPORT_DIRECTORY . BOOKING_DETAILS_FILE, 'r');

    if (!$filteredBookings) {
        die('Could not open file: "' . IMPORT_DIRECTORY . BOOKING_DETAILS_FILE . '"');
    }

    $headers = fgetcsv($filteredBookings);

    $data = [];
    while (($row = fgetcsv($filteredBookings)) !== false) {
        $row = (object)array_combine($headers, $row);

        if (empty($data[$row->{'bookingID'}])) {
            $data[$row->{'bookingID'}][] = (object)[
                'MultiBookingRef' => $row->{'bookingdetailID'},
                'Status' => $row->{'bookingstatus'},
                'bookingenddate' => $row->{'bookingenddate'},
            ];
            $data[$row->{'bookingID'}]['statistics'] = (object)[
                'ConfirmedStatusCount' => 0,
                'CanceledStatusCount' => 0,
                'bookingenddate' => $row->{'bookingenddate'},
            ];

            if (stripos($row->{'bookingstatus'}, 'confirmed') !== false) {
                $data[$row->{'bookingID'}]['statistics']->ConfirmedStatusCount++;
            }
            if (stripos($row->{'bookingstatus'}, 'cancelled') !== false) {
                $data[$row->{'bookingID'}]['statistics']->CanceledStatusCount++;
            }
        } else {
//            print_r([$data[$row->{'bookingID'}], $row->{'bookingID'}]);

            $data[$row->{'bookingID'}][] = (object)[
                'MultiBookingRef' => $row->{'bookingdetailID'},
                'Status' => $row->{'bookingstatus'},
                'bookingenddate' => $row->{'bookingenddate'},
            ];

            if (stripos($row->{'bookingstatus'}, 'confirmed') !== false) {
                $data[$row->{'bookingID'}]['statistics']->ConfirmedStatusCount++;
            }
            if (stripos($row->{'bookingstatus'}, 'cancelled') !== false) {
                $data[$row->{'bookingID'}]['statistics']->CanceledStatusCount++;
            }

//            var_dump($data[$row->{'bookingID'}]);die;
        }
    }

    fclose($filteredBookings);

    foreach ($data as $index => $d) {
        if ($d['statistics']->ConfirmedStatusCount > 1
            //&& $d['statistics']->ConfirmedStatusCount < 5
            //&& $d['statistics']->CanceledStatusCount < 2
            && $d['statistics']->bookingenddate > '2020-10-03 00:00:00'
        ) {
            print_r([$index, $d]);
        }
    }
//print_r($data);
}

function test_checkBookingPaymentsToMoreThanOneOwner()
{
    $expectedResults = (object)[
        'test1' => (object)[
            'value' => true,
            'description' => 'test_01::two-owners => no-refunds => payment-amounts-match',
        ],
        'test2' => (object)[
            'value' => true,
            'description' => 'test_02::two-owners => partial-refund => payment-amounts-don\'t-match',
        ],
        'test3' => (object)[
            'value' => false,
            'description' => 'test_03::two-owners => full-refund => pass',
        ],
        'test4' => (object)[
            'value' => true,
            'description' => 'test_04::three-or-more-owners => no-refunds => pass',
        ],
    ];

    $case_test1 = [
        0 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2019-06-30 00:00:00',
            'managerID' => 140385,
            'paymentcaption' => 'Booking 38211 deposit to owner',
            'owner_payment' => 80,
            'commission_gross' => 20,
        ],

        1 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2019-10-31 00:00:00',
            'managerID' => 140385,
            'paymentcaption' => 'Booking 38211 balance to owner',
            'owner_payment' => 344,
            'commission_gross' => 86,
        ],

        2 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2020-11-30 00:00:00',
            'managerID' => 167418,
            'paymentcaption' => 'Booking 38211 deposit from previous owner',
            'owner_payment' => 80,
            'commission_gross' => 20,
        ],

        3 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2020-11-30 00:00:00',
            'managerID' => 167418,
            'paymentcaption' => 'Booking 38211 balance from previous owner',
            'owner_payment' => 344,
            'commission_gross' => 86,
        ],
    ];

    $test1_result = checkBookingPaymentsToMoreThanOneOwner($case_test1);
    if ($test1_result === $expectedResults->test1->value) {
        echo $expectedResults->test1->description.' => pass'.PHP_EOL;
    } else {
        echo $expectedResults->test1->description.' => FAIL'.PHP_EOL;
    }

    $case_test2 = [
        0 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2019-06-30 00:00:00',
            'managerID' => 140385,
            'paymentcaption' => 'Booking 38211 deposit to owner',
            'owner_payment' => 80,
            'commission_gross' => 20,
        ],

        1 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2019-10-31 00:00:00',
            'managerID' => 140385,
            'paymentcaption' => 'Booking 38211 balance to owner',
            'owner_payment' => 344,
            'commission_gross' => 86,
        ],
        // owner 140385 makes a partial refund
        4 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2019-10-31 00:00:00',
            'managerID' => 140385,
            'paymentcaption' => 'Booking 38211 balance to owner',
            'owner_payment' => -344,
            'commission_gross' => 86,
        ],

        2 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2020-11-30 00:00:00',
            'managerID' => 167418,
            'paymentcaption' => 'Booking 38211 deposit from previous owner',
            'owner_payment' => 80,
            'commission_gross' => 20,
        ],

        3 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2020-11-30 00:00:00',
            'managerID' => 167418,
            'paymentcaption' => 'Booking 38211 balance from previous owner',
            'owner_payment' => 344,
            'commission_gross' => 86,
        ],
    ];

    $test2_result = checkBookingPaymentsToMoreThanOneOwner($case_test2);
    if ($test2_result === $expectedResults->test2->value) {
        echo $expectedResults->test2->description.' => pass'.PHP_EOL;
    } else {
        echo $expectedResults->test2->description.' => FAIL'.PHP_EOL;
    }

    $case_test3 = [
        0 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2019-06-30 00:00:00',
            'managerID' => 140385,
            'paymentcaption' => 'Booking 38211 deposit to owner',
            'owner_payment' => 80,
            'commission_gross' => 20,
        ],

        1 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2019-10-31 00:00:00',
            'managerID' => 140385,
            'paymentcaption' => 'Booking 38211 balance to owner',
            'owner_payment' => 344,
            'commission_gross' => 86,
        ],
        // owner 140385 makes a full refund
        4 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2019-10-31 00:00:00',
            'managerID' => 140385,
            'paymentcaption' => 'Booking 38211 balance to owner',
            'owner_payment' => (-344 - 80),
            'commission_gross' => 86,
        ],

        2 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2020-11-30 00:00:00',
            'managerID' => 167418,
            'paymentcaption' => 'Booking 38211 deposit from previous owner',
            'owner_payment' => 80,
            'commission_gross' => 20,
        ],

        3 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2020-11-30 00:00:00',
            'managerID' => 167418,
            'paymentcaption' => 'Booking 38211 balance from previous owner',
            'owner_payment' => 344,
            'commission_gross' => 86,
        ],
    ];

    $test3_result = checkBookingPaymentsToMoreThanOneOwner($case_test3);
    if ($test3_result === $expectedResults->test3->value) {
        echo $expectedResults->test3->description.' => pass'.PHP_EOL;
    } else {
        echo $expectedResults->test3->description.' => FAIL'.PHP_EOL;
    }

    $case_test4 = [
        0 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2019-06-30 00:00:00',
            'managerID' => 140385,
            'paymentcaption' => 'Booking 38211 deposit to owner',
            'owner_payment' => 80,
            'commission_gross' => 20,
        ],

        1 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2019-10-31 00:00:00',
            'managerID' => 140385,
            'paymentcaption' => 'Booking 38211 balance to owner',
            'owner_payment' => 344,
            'commission_gross' => 86,
        ],

        2 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2020-11-30 00:00:00',
            'managerID' => 167418,
            'paymentcaption' => 'Booking 38211 deposit from previous owner',
            'owner_payment' => 80,
            'commission_gross' => 20,
        ],

        3 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2020-11-30 00:00:00',
            'managerID' => 167418,
            'paymentcaption' => 'Booking 38211 balance from previous owner',
            'owner_payment' => 300,
            'commission_gross' => 86,
        ],

        5 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2020-11-30 00:00:00',
            'managerID' => 167420,
            'paymentcaption' => 'Booking 38211 deposit from previous owner',
            'owner_payment' => 80,
            'commission_gross' => 20,
        ],

        6 => (object) [
            'bookingID' => 102574218,
            'paiddate' => '2020-11-30 00:00:00',
            'managerID' => 167420,
            'paymentcaption' => 'Booking 38211 balance from previous owner',
            'owner_payment' => 200,
            'commission_gross' => 86,
        ],
    ];

    $test4_result = checkBookingPaymentsToMoreThanOneOwner($case_test4);
    if ($test4_result === $expectedResults->test4->value) {
        echo $expectedResults->test4->description.' => pass'.PHP_EOL;
    } else {
        echo $expectedResults->test4->description.' => FAIL'.PHP_EOL;
    }
}

/**
 * Case where owner payments are made to TWO owners
 * - check if the Owner payment amounts match: this means that one of the owners have not refunded the moneys
 * - if the Owner payment amounts DON'T match: then check IF one of the amounts is > zero and < the highest amount
 * => return TRUE if then above conditions are a match
 */
function checkBookingPaymentsToMoreThanOneOwner($individual_owner_payments)
{
    $ownerPaymentsSUM = [];
    foreach ($individual_owner_payments as $ownerpayment) {
        if (empty($ownerPaymentsSUM[$ownerpayment->managerID])) {
            $ownerPaymentsSUM[$ownerpayment->managerID] = (object) [
                'owner_payment' => $ownerpayment->owner_payment,
                'ownerID' => $ownerpayment->managerID,
            ];
        } else {
            $ownerPaymentsSUM[$ownerpayment->managerID]->owner_payment += $ownerpayment->owner_payment;
        }
    }

    $maxPaymentInArray = maxValueInArray($ownerPaymentsSUM, 'owner_payment', 'ownerID');
    foreach ($ownerPaymentsSUM as $ownerPayment) {
        // For every Payment to a different Owner, check if the payment amounts match ->
        // If so: this means that one of the owners have not refunded the moneys
        if ($ownerPayment->ownerID !== $maxPaymentInArray->ownerID &&
            $ownerPayment->owner_payment === $maxPaymentInArray->owner_payment) {
            return true;
        }

        // If the Owner payment amounts DON'T match:
        // Then check IF one of the amounts is > zero and < the highest amount
        if ($ownerPayment->ownerID !== $maxPaymentInArray->ownerID &&
            $ownerPayment->owner_payment !== $maxPaymentInArray->owner_payment &&
            $ownerPayment->owner_payment > 0 && $ownerPayment->owner_payment < $maxPaymentInArray->owner_payment) {
            return true;
        }
    }

    return false;
}

function maxValueInArray($array, $keyToSearch, $keyToIdentify)
{
    $currentMax = null;
    $currentMaxKey = null;
    foreach($array as $arr)
    {
        foreach($arr as $key => $value)
        {
            if ($key == $keyToSearch && ($value >= $currentMax))
            {
                $currentMax = $value;
                $currentMaxKey = $arr->{$keyToIdentify};
            }
        }
    }

    return (object)[
        $keyToSearch => $currentMax,
        $keyToIdentify => $currentMaxKey,
    ];
}

function test_str_replace()
{
    $subject = 'Owner Balances - Deposits';
    $result = str_replace(
        ['-', ' ', '__'],
        ['', '_', '_'],
        strtolower($subject)
    );

    echo $result;
}

