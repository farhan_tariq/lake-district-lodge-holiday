<?php

include_once './dotenv.php';
(new DotEnv(__DIR__ . '/../.env'))->load();

const IMPORT_DIRECTORY = __DIR__ . '/../imports/';
const LDLH = 325;
const DEBTORS_CONTROL_ACCOUNT = 129;

pcntl_async_signals(true);
pcntl_signal(SIGINT, "endCommand");
$run = true;

$conn = initDB();

// Adding a limit to be 233211082 (MAX at 21/April)
// Adding a limit to be 233667056 (MAX at 25/April)
// Adding a limit to be 233823366 (MAX at 26/April)
// Adding a limit to be 233927785 (MAX at 27/April)
// Adding a limit to be 240542517 (MAX at 27/April GoLive Start)
// Leave both calls of getStartEnd_query() function
$startEnd_query = getStartEnd_query(240542517);

$firstQuery_dateStart = new DateTime();
$start_queryResult = $conn->query($startEnd_query);
$firstQuery_timeDiff = $firstQuery_dateStart->diff(new DateTime())->format('%H hours, %I minutes %S seconds');
echo "---" .PHP_EOL.
     "First query took $firstQuery_timeDiff" .PHP_EOL.
     "---" .PHP_EOL;

$start_queryValues = [];
$startMAX_nl_PK = 0;
while ($obj = $start_queryResult->fetch_object()) {
    $translatedFieldName = translateFieldName($obj->nominal_account_type);
    $start_queryValues[$translatedFieldName] = $obj->amount;

    if ((int)$obj->__pk === DEBTORS_CONTROL_ACCOUNT) {
        $start_queryValues['sales_ledger'] = $obj->sales_ledger;
    }

    if ($obj->MAX > $startMAX_nl_PK) {
        $startMAX_nl_PK = $obj->MAX;
    }
}

// convert to object and populate the settled value
$start_queryValues = (object)$start_queryValues;
$start_queryValues->MAX = $startMAX_nl_PK;
$start_queryValues->settled = getSettled_amount($conn, $startMAX_nl_PK);


// Halt process while the import is going
closeDB($conn);
haltProcess();
$conn = initDB();

// If overriding the max nl.__pk on the previous function call of getStartEnd_query()
// Adding a limit to be 240712322 (MAX at 27/April GoLive Start)
// Then leave the following line uncommented
$startEnd_query = getStartEnd_query(240712322);
$secondQuery_dateStart = new DateTime();
$end_queryResult = $conn->query($startEnd_query);
$secondQuery_timeDiff = $secondQuery_dateStart->diff(new DateTime())->format('%H hours, %I minutes %S seconds');
echo "---" .PHP_EOL.
     "Second query took $secondQuery_timeDiff" .PHP_EOL.
     "---" .PHP_EOL;

$end_queryValues = [];
$endMAX_nl_PK = 0;
while ($obj = $end_queryResult->fetch_object()) {
    $translatedFieldName = translateFieldName($obj->nominal_account_type);
    $end_queryValues[$translatedFieldName] = $obj->amount;

    if ((int)$obj->__pk === DEBTORS_CONTROL_ACCOUNT) {
        $end_queryValues['sales_ledger'] = $obj->sales_ledger;
    }

    if ($obj->MAX > $endMAX_nl_PK) {
        $endMAX_nl_PK = $obj->MAX;
    }
}

// convert to object and populate the settled value
$end_queryValues = (object)$end_queryValues;
$end_queryValues->MAX = $endMAX_nl_PK;
$end_queryValues->settled = getSettled_amount($conn, $endMAX_nl_PK);

$importNonImport_query = getImportNonImport_query($startMAX_nl_PK, $endMAX_nl_PK);
$thirdQuery_dateStart = new DateTime();
$importNonImport_queryResult = $conn->query($importNonImport_query);
$thirdQuery_timeDiff = $thirdQuery_dateStart->diff(new DateTime())->format('%H hours, %I minutes %S seconds');
echo "---" .PHP_EOL.
    "Third query took $thirdQuery_timeDiff" .PHP_EOL.
    "---" .PHP_EOL;

$importNonImport_queryValues = (object)[
    'nonImport' => (object)[],
    // During the tests there might not be results for LDLH so we add an object with zero values
    'import' => (object)[]
];
while ($obj = $importNonImport_queryResult->fetch_object()) {
    if (empty($obj->_fk_brand)) {
        continue;
    }

    if ((int)$obj->_fk_brand === LDLH) {
        foreach ($obj as $fieldName => $value) {
            // There will be only one line for LDLH so use =
            $importNonImport_queryValues->import->$fieldName = $value;
        }
    } else {
        foreach ($obj as $fieldName => $value) {
            if (array_key_exists($fieldName, $importNonImport_queryValues->nonImport)) {
                $importNonImport_queryValues->nonImport->$fieldName += $value;
            } else {
                $importNonImport_queryValues->nonImport->$fieldName = $value;
            }
        }
    }
}


$bookingsImported = getBookingsImported($conn);
$propertyStatusCount = getPropertyStatusCount($conn);

$details_queryValues = (object)[
    'bookings_imported' => $bookingsImported->bookings_imported,
    'live_properties' => $propertyStatusCount->live_properties,
    'withdrawing_properties' => $propertyStatusCount->withdrawing_properties,
];

// DEBUG DATA
print_r([
    'start_queryValues' => $start_queryValues,
    'end_queryValues' => $end_queryValues,
    'importNonImport_queryValues' => $importNonImport_queryValues,
    'details' => $details_queryValues,
]);

$graphTemplate = readGraphTemplate();
$graphData = replace_Import_and_NonImport_placeholders(
    $graphTemplate,
    $importNonImport_queryValues
);
$graphData = replace_StartEnd_placeholders(
    $graphData,
    $start_queryValues,
    $end_queryValues,
    $details_queryValues
);

writeGraphTemplate($graphData);

closeDB($conn);


function initDB()
{
    $servername = getenv('DB_OPTEST_SERVERNAME');
    $username = getenv('DB_OPTEST_USERNAME');
    $password = getenv('DB_OPTEST_PASSWORD');
    $database = getenv('DB_OPTEST_NAME');

    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $database);

    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    echo 'Connected' . PHP_EOL;
    return $conn;
}

function closeDB($conn)
{
    if ($conn) {
        $conn->close();
    }
}

/**
 * $nl_initialPK = 232145795
 * This is the minimum nl.__pk from OP Test for LDLH.
 * While there's no integration, this can be used for testing purpose
 */
function getImportNonImport_query($nl_initialPK, $nl_endPK)
{
    $sql = "SELECT
      o.`_fk_brand`,
      SUM(
        IF (
        nominal_account_type_id = 129,
          nl.amount_sterling,
          0
        )
      ) AS debtors_control_account,
      SUM(
        IF (
        nominal_account_type_id = 3371,
          nl.amount_sterling,
          0
        )
      ) AS debters_2,
      SUM(
        IF (
        nominal_account_type_id = 130,
          nl.amount_sterling,
          0
        )
      ) AS sykes_deferred,
      SUM(
        IF (
        nominal_account_type_id = 131,
          nl.amount_sterling,
          0
        )
      ) AS owner_deferred,
      SUM(
        IF (
        nominal_account_type_id = 133,
          nl.amount_sterling,
          0
        )
      ) AS bank_current_account,
      SUM(
        IF (
        nominal_account_type_id = 139,
          nl.amount_sterling,
          0
        )
      ) AS owner_balances_deposits,
      SUM(
        IF (
        nominal_account_type_id = 142,
          nl.amount_sterling,
          0
        )
      ) AS sales_tax_control_account,
      SUM(
        IF (
        nominal_account_type_id = 145,
          nl.amount_sterling,
          0
        )
      ) AS commission,
      SUM(
        IF (
        nominal_account_type_id = 160,
          nl.amount_sterling,
          0
        )
      ) AS booking_fees,
      SUM(
        IF (
          _fk_line_type = 8,
          nl.amount_sterling,
          0
        )
      ) AS sales_ledger,
      SUM(
        IF (
        nominal_account_type_id = 205,
          nl.amount_sterling,
          0
        )
      ) AS adhoc_fees,
      SUM(
        IF (
          nominal_account_type_id = 220,
          nl.amount_sterling,
          0
        )
      ) AS adjustments,
      nl.`_fk_itinerary` AS sykes_ref,
      eim.`acquisition_id` AS ll_ref,
      eim_owner.`acquisition_id` AS tabs_owner,
      SUM(sykes_finance.`fn_getInvoiceAmountSettled`(nl.__pk)) AS settled
    FROM
      sykes_finance.nominal_ledger nl
      LEFT JOIN toms.`owners` o
        ON o.`__pk` = nl.`_fk_owner`
      LEFT JOIN acquisitions.`entity_id_mapping` eim
        ON eim.`sykes_entity_id` = nl.`_fk_itinerary`
          AND eim.`entity_type` = 'booking'
          AND eim.`_fk_brand` = 325
      LEFT JOIN acquisitions.`entity_id_mapping` eim_owner
        ON eim_owner.`sykes_entity_id` = nl.`_fk_owner`
          AND eim_owner.`entity_type` = 'owner'
          AND eim_owner.`_fk_brand` = 325
      LEFT JOIN sykes_platform.`brands` b
        ON b.`__pk` = o.`_fk_brand`
    WHERE nl.`__pk` > $nl_initialPK
      AND nl.`__pk` <= $nl_endPK
    GROUP BY o.`_fk_brand` = 325";

    return $sql;
}


function getStartEnd_query($limitRecords = null)
{
    $limit = '';
    if (!empty($limitRecords)) {
        $limit = " WHERE nl.`__pk` <= $limitRecords ";
    }

    $sql = "SELECT
      lo.`__pk`,
      lo.`name` nominal_account_type,
      SUM(nl.`amount_sterling`) AS amount,
      SUM(IF (nl.`_fk_line_type` = 8, nl.`amount_sterling`, 0)) AS sales_ledger,

      MAX(nl.`transaction_date`),
      MAX(nl.`__pk`) AS MAX
    FROM
      sykes_finance.`nominal_ledger` nl
    LEFT JOIN sykes_platform.`lookup_options` lo
      ON lo.`__pk` = nl.`nominal_account_type_id`
    $limit
    GROUP BY nl.`nominal_account_type_id`";

    return $sql;
}


function getSettled_query($max_nl_PK)
{
    $sql = "SELECT
      SUM(nls.`amount_sterling`) AS settled
    FROM
      sykes_finance.`nominal_ledger_settlement` nls
    WHERE
      nls.`_fk_nominal_ledger` <= $max_nl_PK";

    return $sql;
}


function getSettled_amount($conn, $max_nl_PK)
{
    $settled_query = getSettled_query($max_nl_PK);
    $settled_queryResult = $conn->query($settled_query);
    $settled_queryValues = $settled_queryResult->fetch_object();

    return $settled_queryValues->settled ?? 0;
}

function getBookingsImported($conn)
{
    $sql = "SELECT
      COUNT(i.`nitineraryid`) AS bookings_imported
    FROM
      sykes_reservations.`itinerary` i
    JOIN toms.`properties` p ON p.`__pk` = i.`npropertyid`
    WHERE
      p.`_fk_brand` = ".LDLH;

    $queryResult = $conn->query($sql);
    return $queryResult->fetch_object();
}

function getPropertyStatusCount($conn)
{
    $sql = "SELECT
      COUNT(IF (p.`live_status` = 'C', 1, NULL)) AS live_properties,
      COUNT(IF (p.`live_status` = 'G', 1, NULL)) AS withdrawing_properties
    FROM
        toms.`properties` p
    WHERE
      p.`_fk_brand` = ".LDLH;

    $queryResult = $conn->query($sql);
    return $queryResult->fetch_object();
}


function readGraphTemplate()
{
    $graphTemplate = file_get_contents('./graph.dot.template');
    if (!$graphTemplate) {
        die('Could not open file: "./graph.dot.template"');
    }

    return $graphTemplate;
}

/**
 * NOMINAL_ACCOUNT_TYPES = [
 *  129 => 'debtors_control_account' (debtors)
 *  3371 => 'debters_2' (debtors2)
 *  130 => 'sykes_deferred',
 *  131 => 'owner_deferred',
 *  133 => 'bank_current_account' (bank)
 *  139 => 'owner_balances_deposits' (owner_balances),
 *  142 => 'sales_tax_control_account' ('sales_tax')
 *  145 => 'commission'
 *  160 => 'booking_fees'
 *  205 => 'adhoc_fees' (brand_fee)
 *  220 => 'adjustments' (adhocs)
 * ]
 *
 * sales_ledger => separate query
 * settled => separate query
 */
function replace_Import_and_NonImport_placeholders($graphData, $importNonImport_queryValues)
{
    $curr = '£';
    $replacePlaceholders = [
        'SALES_LEDGER_IMPORT' => $curr.number_format($importNonImport_queryValues->import->sales_ledger ?? 0, 2),
        'SALES_LEDGER_NON_IMPORT' => $curr.number_format($importNonImport_queryValues->nonImport->sales_ledger ?? 0, 2),

        'BANK_IMPORT' => $curr.number_format($importNonImport_queryValues->import->bank_current_account ?? 0, 2),
        'BANK_NON_IMPORT' => $curr.number_format($importNonImport_queryValues->nonImport->bank_current_account ?? 0, 2),

        'OWNER_DEFERRED_IMPORT' => $curr.number_format($importNonImport_queryValues->import->owner_deferred ?? 0, 2),
        'OWNER_DEFERRED_NON_IMPORT' => $curr.number_format($importNonImport_queryValues->nonImport->owner_deferred ?? 0, 2),

        'OWNER_BALANCES_IMPORT' => $curr.number_format($importNonImport_queryValues->import->owner_balances_deposits ?? 0, 2),
        'OWNER_BALANCES_NON_IMPORT' => $curr.number_format($importNonImport_queryValues->nonImport->owner_balances_deposits ?? 0, 2),

        'SETTLED_IMPORT' => $curr.number_format($importNonImport_queryValues->import->settled ?? 0, 2),
        'SETTLED_NON_IMPORT' => $curr.number_format($importNonImport_queryValues->nonImport->settled ?? 0, 2),

        'DEBTORS_IMPORT' => $curr.number_format($importNonImport_queryValues->import->debtors_control_account ?? 0, 2),
        'DEBTORS_NON_IMPORT' => $curr.number_format($importNonImport_queryValues->nonImport->debtors_control_account ?? 0, 2),

        'DEBTORS2_IMPORT' => $curr.number_format($importNonImport_queryValues->import->debters_2 ?? 0, 2),
        'DEBTORS2_NON_IMPORT' => $curr.number_format($importNonImport_queryValues->nonImport->debters_2 ?? 0, 2),

        'SYKES_DEFERRED_IMPORT' => $curr.number_format($importNonImport_queryValues->import->sykes_deferred ?? 0, 2),
        'SYKES_DEFERRED_NON_IMPORT' => $curr.number_format($importNonImport_queryValues->nonImport->sykes_deferred ?? 0, 2),

        'COMMISSION_IMPORT' => $curr.number_format($importNonImport_queryValues->import->commission ?? 0, 2),
        'COMMISSION_NON_IMPORT' => $curr.number_format($importNonImport_queryValues->nonImport->commission ?? 0, 2),

        'BOOKING_FEE_IMPORT' => $curr.number_format($importNonImport_queryValues->import->booking_fees ?? 0, 2),
        'BOOKING_FEE_NON_IMPORT' => $curr.number_format($importNonImport_queryValues->nonImport->booking_fees ?? 0, 2),

        'SALES_TAX_IMPORT' => $curr.number_format($importNonImport_queryValues->import->sales_tax_control_account ?? 0, 2),
        'SALES_TAX_NON_IMPORT' => $curr.number_format($importNonImport_queryValues->nonImport->sales_tax_control_account ?? 0, 2),

        'ADHOCS_IMPORT' => $curr.number_format($importNonImport_queryValues->import->adjustments ?? 0, 2),
        'ADHOCS_NON_IMPORT' => $curr.number_format($importNonImport_queryValues->nonImport->adjustments ?? 0, 2),

        'BRAND_FEE_IMPORT' => $curr.number_format($importNonImport_queryValues->import->adhoc_fees ?? 0, 2),
        'BRAND_FEE_NON_IMPORT' => $curr.number_format($importNonImport_queryValues->nonImport->adhoc_fees ?? 0, 2),
    ];

    $updatedGraphData = $graphData;
    foreach ($replacePlaceholders as $placeholder => $value) {
        $updatedGraphData = str_replace($placeholder, $value, $updatedGraphData);
    }

    return $updatedGraphData;
}

function replace_StartEnd_placeholders($graphData, $start_queryValues, $end_queryValues, $details)
{
    $curr = '£';
    $replacePlaceholders = [
        'SALES_LEDGER_START' => $curr.number_format($start_queryValues->sales_ledger, 2),
        'SALES_LEDGER_END' => $curr.number_format($end_queryValues->sales_ledger, 2),
        'SALES_LEDGER_DIFF' => $curr.number_format($end_queryValues->sales_ledger - $start_queryValues->sales_ledger, 2),

        'BANK_START' => $curr.number_format($start_queryValues->bank_current_account, 2),
        'BANK_END' => $curr.number_format($end_queryValues->bank_current_account, 2),
        'BANK_DIFF' => $curr.number_format($end_queryValues->bank_current_account - $start_queryValues->bank_current_account, 2),

        'DETAILS_START_PK' => number_format($start_queryValues->MAX, 0, '.', ' '),
        'DETAILS_END_PK' => number_format($end_queryValues->MAX, 0, '.', ' '),
        'DETAILS_BOOKINGS_IMPORTED' => number_format($details->bookings_imported, 0, '.', ' '),
        'DETAILS_IMPORTED_LIVE_PROPERTIES' => number_format($details->live_properties, 0, '.', ' '),
        'DETAILS_IMPORTED_WITHDRAWING_PROPERTIES' => number_format($details->withdrawing_properties, 0, '.', ' '),

        'OWNER_DEFERRED_START' => $curr.number_format($start_queryValues->owner_deferred, 2),
        'OWNER_DEFERRED_END' => $curr.number_format($end_queryValues->owner_deferred, 2),
        'OWNER_DEFERRED_DIFF' => $curr.number_format($end_queryValues->owner_deferred - $start_queryValues->owner_deferred, 2),

        'OWNER_BALANCES_START' => $curr.number_format($start_queryValues->owner_balances_deposits, 2),
        'OWNER_BALANCES_END' => $curr.number_format($end_queryValues->owner_balances_deposits, 2),
        'OWNER_BALANCES_DIFF' => $curr.number_format($end_queryValues->owner_balances_deposits - $start_queryValues->owner_balances_deposits, 2),

        'SETTLED_START' => $curr.number_format($start_queryValues->settled, 2),
        'SETTLED_END' => $curr.number_format($end_queryValues->settled, 2),
        'SETTLED_DIFF' => $curr.number_format($end_queryValues->settled - $start_queryValues->settled, 2),

        'DEBTORS_START' => $curr.number_format($start_queryValues->debtors_control_account, 2),
        'DEBTORS_END' => $curr.number_format($end_queryValues->debtors_control_account, 2),
        'DEBTORS_DIFF' => $curr.number_format($end_queryValues->debtors_control_account - $start_queryValues->debtors_control_account, 2),

        'DEBTORS2_START' => $curr.number_format($start_queryValues->debters_2 ?? 0, 2),
        'DEBTORS2_END' => $curr.number_format($end_queryValues->debters_2 ?? 0, 2),
        'DEBTORS2_DIFF' => $curr.number_format(($end_queryValues->debters_2 ?? 0) - ($start_queryValues->debters_2 ?? 0), 2),

        'SYKES_DEFERRED_START' => $curr.number_format($start_queryValues->sykes_deferred, 2),
        'SYKES_DEFERRED_END' => $curr.number_format($end_queryValues->sykes_deferred, 2),
        'SYKES_DEFERRED_DIFF' => $curr.number_format($end_queryValues->sykes_deferred - $start_queryValues->sykes_deferred, 2),

        'COMMISSION_START' => $curr.number_format($start_queryValues->commission, 2),
        'COMMISSION_END' => $curr.number_format($end_queryValues->commission, 2),
        'COMMISSION_DIFF' => $curr.number_format($end_queryValues->commission - $start_queryValues->commission, 2),

        'BOOKING_FEE_START' => $curr.number_format($start_queryValues->booking_fees, 2),
        'BOOKING_FEE_END' => $curr.number_format($end_queryValues->booking_fees, 2),
        'BOOKING_FEE_DIFF' => $curr.number_format($end_queryValues->booking_fees - $start_queryValues->booking_fees, 2),

        'SALES_TAX_START' => $curr.number_format($start_queryValues->sales_tax_control_account, 2),
        'SALES_TAX_END' => $curr.number_format($end_queryValues->sales_tax_control_account, 2),
        'SALES_TAX_DIFF' => $curr.number_format($end_queryValues->sales_tax_control_account - $start_queryValues->sales_tax_control_account, 2),

        'ADHOCS_START' => $curr.number_format($start_queryValues->adjustments ?? 0, 2),
        'ADHOCS_END' => $curr.number_format($end_queryValues->adjustments ?? 0, 2),
        'ADHOCS_DIFF' => $curr.number_format(($end_queryValues->adjustments ?? 0) - ($start_queryValues->adjustments ?? 0), 2),

        'BRAND_FEE_START' => $curr.number_format($start_queryValues->adhoc_fees ?? 0, 2),
        'BRAND_FEE_END' => $curr.number_format($end_queryValues->adhoc_fees ?? 0, 2),
        'BRAND_FEE_DIFF' => $curr.number_format(($end_queryValues->adhoc_fees ?? 0) - ($start_queryValues->adhoc_fees ?? 0), 2),
    ];

    $updatedGraphData = $graphData;
    foreach ($replacePlaceholders as $placeholder => $value) {
        $updatedGraphData = str_replace($placeholder, $value, $updatedGraphData);
    }

    return $updatedGraphData;
}

function translateFieldName($subject)
{
    return str_replace(
        ['-', ' ', '__'],
        ['', '_', '_'],
        strtolower($subject)
    );
}

function writeGraphTemplate($graphData)
{
    $filename = IMPORT_DIRECTORY . 'graph.' . date('Ymd') . '.dot';
    file_put_contents($filename, $graphData);
    echo PHP_EOL."-----".PHP_EOL."Created graph file: $filename".PHP_EOL;
}

//function writeGraphStartData($start_queryValues, $importNonImport_queryValues)
//{
//    $filename = IMPORT_DIRECTORY.'graph_start_data.'.date('Ymd').'.json';
//    $fp = fopen($filename, 'w');
//    fwrite(
//        $fp,
//        json_encode([
//            'start_queryValues' => $start_queryValues,
//            'importNonImport_queryValues' => $importNonImport_queryValues,
//        ])
//    );
//    fclose($fp);
//    echo "'Created file '$filename'".PHP_EOL;
//}

//function readGraphStartData()
//{
//    $filename = IMPORT_DIRECTORY . 'graph_start_data.' . date('Ymd') . '.json';
//    $fileData = file_get_contents($filename);
//    $graphStartData = json_decode($fileData);
//    if (!empty($graphStartData)) {
//        return $graphStartData;
//    }
//
//    return json_decode('{}');
//}

function haltProcess()
{
    global $run;

    echo "Waiting for signal interrupt".PHP_EOL;
    $dateStart = new DateTime();
    echo $dateStart->format('Y-m-d H:i') . PHP_EOL;
    echo "Press control c to finish" . PHP_EOL;

    while ($run) {
        sleep(900);
        if ($run) {
            $runningDate = $dateStart->diff(new DateTime())->format('%H hours, %I minutes %S seconds');
            echo "waiting $runningDate" . PHP_EOL;
        }
    }
    echo PHP_EOL."Ending halt process".PHP_EOL;
    $dateEnd = new DateTime();
    $intervalDiff = $dateStart->diff($dateEnd);
    echo "Waiting time taken".PHP_EOL.
        $intervalDiff->format('%H hours, %I minutes %S seconds').PHP_EOL;
}

function endCommand()
{
    $GLOBALS['run'] = false;
}
