<?php

const MAX_ERRORS = 5;
const MAX_SAMPLE_SIZE = 100;
const COMMISSION_PERCENTAGE_THRESHOLD = 0.20;

function saveDataToFile($fileName, $headerLine, $fileData) {
    $fileStream = fopen($fileName, 'w');

    if (!$fileStream) {
        echo PHP_EOL.'ERROR - Could not open file "'.$fileName.'"'.PHP_EOL;
        return;
    }

    echo PHP_EOL.'Opened file for writing: "'.$fileName.'"'.PHP_EOL;

    // Write header
    fputcsv($fileStream, $headerLine);

    // Write data
    foreach ($fileData as $data) {
        fputcsv($fileStream, $data);
    }

    fclose($fileStream);

    echo 'Finished writing data and Closed file!'.PHP_EOL;
}


/**
 * Reads the data from a file
 * @param $fileName
 * @param array $fields
 * @param null $customArrayKey
 * @param string $options Requires the $customArrayKey. Available options: ['group', 'sum', ...]
 * 'sum' option is not yet implemented. If not needed, then can be removed from the documentation.
 * @return array
 */
function getDataFromFile($fileName, $fields = [], $customArrayKey = null, $options = null) {
    $csv = fopen($fileName, 'r');
    if (!$csv) {
        die('Could not open file: "'.$fileName.'"');
    }

    echo 'Reading file:    "'.$fileName.'"'.PHP_EOL;

    $headers = fgetcsv($csv); // get rid of header line

    $totalRows = 0;
    $data = [];
    while ($row = fgetcsv($csv)) {
        $row = array_combine($headers, $row);

        // If custom $fields have been passed, use them
        if (!empty($fields)) {
            $currentRowData = [];
            foreach ($fields as $field) {
                if (isset($row[$field])) {
                    $currentRowData[$field] = $row[$field];
                }
            }

            if (!empty($customArrayKey) && !empty($row[$customArrayKey])) {
                if (!empty($options) && $options === 'group') {
                    $data[$row[$customArrayKey]][] = $currentRowData;
                } else {
                    $data[$row[$customArrayKey]] = $currentRowData;
                }
            } else {
                $data[] = $currentRowData;
            }
        } else { // If there's no custom fields then use the original fields
            if (!empty($customArrayKey) && !empty($row[$customArrayKey])) {
                $data[$row[$customArrayKey]] = $row;
            } else {
                $data[] = $row;
            }
        }

        $totalRows++;
    }

    fclose($csv);

    echo 'Finished reading "'.$fileName.'"'.PHP_EOL;
    echo $totalRows.' lines read'.PHP_EOL;
    $linesWritten = count($data);
    echo $linesWritten.' lines written'.PHP_EOL;
    if ($linesWritten != $totalRows) {
        echo 'Possible Duplicate '.($customArrayKey ?? '').' IDs in file!'.PHP_EOL;
        sleep(5);
    }

    return $data;
}

/**
 * Validation checks for bookings. See if the booking price breakdown is adding up correctly.
 */
function validateBookings() {
    $filePath = __DIR__ . '/../imports/sykes-Lake-District-Lodge-Holidays-export-bookings_2021-01-21.csv';
    $csv = fopen($filePath, 'r');
    if (!$csv) {
        die('Could not open file: "'.$filePath.'"');
    }

    $excelLine = 1;
    fgetcsv($csv); // get rid of header line
    $excelLine++;

    $errorsCount = 0;
    $failedConditions = [];
    while ($row = fgetcsv($csv)) {
        list(
            $owner_bookingID,
            $bookingID,
            $cottageID,
            $customerID,
            $bookingdate,
            $bookingstartdate,
            $bookingenddate,
            $bookingstatus,
            $bookingadults,
            $bookingchildren,
            $bookinginfants,
            $total_price,
            $total_rent,
            $booking_fee_price,
            $credit_card_fee_amount,
            $voucher,
            $cancellation_fee,
            $refund,
            $third_party_agent,
            $extras_price,
            $extras_commission,
            $security_deposit_amount,
            $due_to_owner,
            $paid_to_owner,
            $deposit_amount,
            $deposit_due_date,
            $balance_due_date,
            $security_deposit_due_date,
            $security_deposit_paid,
            $notes,
            $sourceID
            ) = $row;

        if ($bookingstatus === 'cancelled') {
            continue;
        }

        // Check condition A: $total_price =
        //         $total_rent + $booking_fee_price + $credit_card_fee_amount + $extras_price + $security_deposit_amount
        //       - $voucher - $refund - $third_party_agent
        $totalPriceCalculation =
            $total_rent + $booking_fee_price + $credit_card_fee_amount + $extras_price + $security_deposit_amount
            - $voucher - $refund - $third_party_agent;
        $totalPriceCalculation = round($totalPriceCalculation, 2);
        if ($total_price != $totalPriceCalculation) {
            $failedConditions[$owner_bookingID] =
                [
                'Excel Row' => $excelLine,
                'Total price VS Calculated price' => $total_price . ' != ' . $totalPriceCalculation,
                'Booking Status' => $bookingstatus,
                '+ total_rent' => $total_rent,
                '+ booking_fee_price' => $booking_fee_price,
                '+ credit_card_fee_amount' => $credit_card_fee_amount,
                '+ extras_price' => $extras_price,
                '+ security_deposit_amount' => $security_deposit_amount,
                '- voucher' => $voucher,
                '- refund' => $refund,
                '- third_party_agent' => $third_party_agent
                ];

            $errorsCount++;
        }

        // Check condition B: check the booking from an Owners point of view
        // current payments


        // Check condition C: check the booking from Sykes point of view
        //

        $excelLine++;

        if ($errorsCount >= MAX_ERRORS) {
            print_r($failedConditions);
            die();
        }
    }

    fclose($csv);
}


/**
 * Adds up the payments for each booking and create a condensed report so that each line has the sum of payments
 * per booking. There will also an indication of which booking has multiple payments and refunds.
 */
function processPayments() {
    $filePath = __DIR__ . '/../imports/sykes-Lake-District-Lodge-Holidays-export-booking-payments_2021-01-21.csv';
    $exportedFile = __DIR__ . '/../exports/NEW_sykes-Lake-District-Lodge-Holidays-export-booking-payments_2021-01-21.csv';
    $csv = fopen($filePath, 'r');
    if (!$csv) {
        die('Could not open file: "'.$filePath.'"');
    }

    $headerLine = fgetcsv($csv); // get rid of header line

    $totalPayments = 0;
    $paymentDataCondensed = [];
    while ($row = fgetcsv($csv)) {
        list(
            $paymentID,
            $paymentdate,
            $bookingID,
            $paymentmethod,
            $paymenttotal
            ) = $row;

        if (!empty($paymentDataCondensed[$bookingID])) {
            $paymentDataCondensed[$bookingID]['paymenttotal'] += (float)$paymenttotal;

            if (empty($paymentDataCondensed[$bookingID]['multiple_payments'])) {
                $paymentDataCondensed[$bookingID]['multiple_payments'] = 2;
            } else {
                $paymentDataCondensed[$bookingID]['multiple_payments']++;
            }

            if ((float)$paymenttotal < 0 && empty($paymentDataCondensed[$bookingID]['have_refund_payment'])) {
                $paymentDataCondensed[$bookingID]['have_refund_payment'] = 1;
            } elseif ((float)$paymenttotal < 0) {
                $paymentDataCondensed[$bookingID]['have_refund_payment']++;
            }
        } else {
            $paymentDataCondensed[$bookingID] = [
                'paymentID' => $paymentID,
                'paymentdate' => $paymentdate,
                'bookingID' => $bookingID,
                'paymentmethod' => $paymentmethod,
                'paymenttotal' => (float)$paymenttotal,
                'multiple_payments' => '',
                'have_refund_payment' => '',
            ];
        }

        $totalPayments += (float)$paymenttotal;
    }

    fclose($csv);


    echo 'Total payment records condensed: '.count($paymentDataCondensed)."\n".
    'Total payment amount: '.$totalPayments."\n";

    // Write data to the new file
    $fileStream = fopen($exportedFile, 'w');

    // Write header
    $headerLine[] = 'multiple_payments';
    $headerLine[] = 'have_refund_payment';
    fputcsv($fileStream, $headerLine);

    // Write data
    foreach ($paymentDataCondensed as $paymentData) {
        fputcsv($fileStream, $paymentData);
    }
    fclose($fileStream);

    echo 'Finished writing file';
}


/**
 * Checks what commission is defined in the properties file
 * @param $propertyId The Property Identifier
 * @return double
 */
function getPropertyCommission($propertyId) {
    $commissionNotFound = 0.0;

    if (empty($propertyId)) {
        echo PHP_EOL.'ERROR: Invalid propertyId passed!'.PHP_EOL;
        return $commissionNotFound;
    }

    // all have an empty value on the commission except 400445 and 545181
    // all have property_status as archived except 58089 and 94438
    // -----
    // this list can be used to allow skipping those properties if errors come up on the reports
    // and they can be analysed separately
    $propertiesWithInvalidCommission = [
        58089, // property_status is disabled
        58171,
        94438, // property_status is disabled
        400445, // commision -1
        414823,
        489891,
        545181, // commission 0
        546607,
        552847,
        556468,
        557351,
        560592,
        565864,
        576011,
        576228
    ];

    $filePath = __DIR__ . '/../imports/sykes-Lake-District-Lodge-Holidays-export-properties_2021-01-21.csv';
    $csv = fopen($filePath, 'r');
    if (!$csv) {
        die('Could not open file: "'.$filePath.'"');
    }

    fgetcsv($csv); // get rid of header line

    while ($row = fgetcsv($csv)) {
        list(
            $cottageID, $managerID, $url, $cottagename, $changeover_day, $pets, $accommodates,
            $cottagebedrooms, $rating, $rating_type, $accepts_short_breaks, $cottageaddress, $cottagetown,
            $cottagepostcode, $cottagecountry, $latitude, $longitude, $short_description, $long_description,
            $location_description, $live_date, $lapse_date, $property_status, $security_desposit_amount, $cleanerID,
            $commission_rate
            ) = $row;

        if ((int)$cottageID === (int)$propertyId) {
            return $commission_rate;
        }
    }

    fclose($csv);

    return $commissionNotFound;
}


/**
 * STEP 01
 * Sums the booking-finances and creates a new file with one row per booking
 */
function bookingsReportStep1() {
    $filePath = __DIR__ . '/../imports/STEP_1-sykes-Lake-District-Lodge-Holidays-export-booking-finances_2021-02-27.csv';
    $csv = fopen($filePath, 'r');
    if (!$csv) {
        die('Could not open file: "'.$filePath.'"');
    }

    $headers = fgetcsv($csv); // get rid of header line

    $totalRows = 0;
    $bookingsCount = 0;
    $financesPerBooking = [];
    while ($row = fgetcsv($csv)) {
        $row = array_combine($headers, $row);
        $currentBookingID = $row['owner_bookingID'];

        $commission = (float)$row['commission'] + (float)$row['commission_vat'];
        $commission_from_extras = (float)$row['other_extras_vat'] + (float)$row['other_extras_net'];
        $commission_total = (float)$row['other_extras_vat'] + (float)$row['other_extras_net'] + $commission;

        // Separate donations from the booking fee
        $bookingFeeVat = 0.0;
        $fixTheFellsDonation = 0.0;
        if (preg_match('/^Fix the Fells - please donate/', $row['paymentcaption'])) {
            $fixTheFellsDonation = (float)$row['booking_fee_vat'];
        } else {
            $bookingFeeVat = (float)$row['booking_fee_vat'];
        }

        // Remove the extras from the Rent (owner_due): Dogs
        $ownerDue = 0.0;
        $extrasDogs = 0.0;
        if (preg_match('/^Dogs/', $row['paymentcaption'])) {
            $extrasDogs = (float)$row['owner_due'];
        } else {
            $ownerDue = (float)$row['owner_due'];
        }

        if (empty($financesPerBooking[$currentBookingID])) {
            $financesPerBooking[$currentBookingID] =
                [
                    'Booking ID' => $currentBookingID,
                    'PropertyRef' => $row['cottageID'],
                    'owner_due' => $ownerDue,
                    'Commission' => $commission,
                    'VAT on commission' => (float)$row['commission_vat'],
                    'booking_fee' => (float)$row['booking_fee'],
                    'booking_fee_vat' => $bookingFeeVat,
                    'Fix the fells donation' => $fixTheFellsDonation,
                    'admin_fee' => (float)$row['admin_fee'],
                    'Extras dogs' => $extrasDogs,
                    'Commission from Extras' => $commission_from_extras,
                    'VAT on Extras commission' => (float)$row['other_extras_net'],
                    'Commission Total' => $commission_total,
                ];

            $bookingsCount++;
        } else {
            $financesPerBooking[$currentBookingID]['owner_due'] += $ownerDue;
            $financesPerBooking[$currentBookingID]['Commission'] += $commission;
            $financesPerBooking[$currentBookingID]['VAT on commission'] += (float)$row['commission_vat'];
            $financesPerBooking[$currentBookingID]['booking_fee'] += (float)$row['booking_fee'];
            $financesPerBooking[$currentBookingID]['booking_fee_vat'] += $bookingFeeVat;
            $financesPerBooking[$currentBookingID]['Fix the fells donation'] += $fixTheFellsDonation;
            $financesPerBooking[$currentBookingID]['admin_fee'] += (float)$row['admin_fee'];
            $financesPerBooking[$currentBookingID]['Extras dogs'] += $extrasDogs;
            $financesPerBooking[$currentBookingID]['Commission from Extras'] += $commission_from_extras;
            $financesPerBooking[$currentBookingID]['VAT on Extras commission'] += (float)$row['other_extras_net'];
            $financesPerBooking[$currentBookingID]['Commission Total'] += $commission_total;
        }

        $totalRows++;
    }

    fclose($csv);

//    $headersToSave = [
//        'Booking ID',
//        'PropertyRef',
//        'owner_due',
//        'Commission',
//        'VAT on commission',
//        'booking_fee',
//        'booking_fee_vat',
//        'Fix the fells donation',
//        'admin_fee',
//        'Extras dogs',
//        'Commission from Extras',
//        'VAT on Extras commission',
//        'Commission Total',
//    ];
//    saveDataToFile($exportedFile, $headersToSave, $financesPerBooking);

    bookingsReportStep2($financesPerBooking);
}


/**
 * STEP 02
 * Checks that this holds true for all bookings
 * OWNER_DUE + COMMISSION_VAT + COMMISSION = TOTAL_RENT from the booking file
 *
 * Creates an updated booking-finances with additional columns, total rent and total price
 * Reads information from the Bookings file and adds all of it to the Report for Finance
 */
function bookingsReportStep2($financesPerBooking) {
    $bookings_filePath = __DIR__ . '/../imports/sykes-Lake-District-Lodge-Holidays-export-bookings_2021-02-27.csv';
    // Removed 3 duplicate rows from the ExportBookings file with owner_BookingID: 2325, 11730 and 22981

    $bookingsFieldsToExtract = [
        'owner_bookingID',
        'bookingID',
        'total_rent',

        'total_price',
        'booking_fee_price',
        'security_deposit_amount',
        'security_deposit_paid',
        // "grand total" = 'total_price + booking_fee_price',

        'paid_to_owner',
        'due_to_owner',
        // "owner deposit due" = 'due_to_owner – paid_to_owner',

        'bookingstatus',
        'cottageID',

        'bookingdate',
        'bookingstartdate',
        'bookingenddate',

        'sourceID',

        'credit_card_fee_amount',
        'extras_price',
        'voucher',
        'refund',
        'third_party_agent',
    ];
    $customArrayKey = 'owner_bookingID';
    $bookingsData = getDataFromFile($bookings_filePath, $bookingsFieldsToExtract, $customArrayKey);

    $countCorrectRent = 0;
    $data = [];
    foreach ($financesPerBooking as $row) {
        $currentOwnerBookingID = $row['Booking ID'];
        $currentBookingRow = $bookingsData[$currentOwnerBookingID];

        // FILTER 1 - Only show bookings where the start data is after 02-10-2020
        if ($currentBookingRow['bookingenddate'] < '2020-10-02') {
            continue;
        }

        // Test 01: Check owner_bookingID
//        if (empty($currentBookingRow)) {
//            echo 'Test 01 failed: Expected "'.$row['Booking ID'].'" | Result: Not Found!'.PHP_EOL;
//        }
        // Test 02: Check cottageID match
//        if (!empty($currentBookingRow) && $row['PropertyRef'] !== $currentBookingRow['cottageID']) {
//            echo 'Test 02 failed: Expected '.$row['PropertyRef'].' | Found '.$currentBookingRow['cottageID'].PHP_EOL;
//        }
        // Test 03: Validate OWNER_DUE + COMMISSION = TOTAL_RENT
        $currentRowTotalRent = (float)$row['owner_due'] + (float)$row['Commission'];

        $totalRentFromBookingsData = $currentBookingRow['total_rent'];

//        if ($currentRowTotalRent != $totalRentFromBookingsData) {
//            echo 'Test 03 failed: Expected '.$currentRowTotalRent.' | Found '.$totalRentFromBookingsData.PHP_EOL;
////            print_r($row); die;
//        } else {
//            $countCorrectRent++;
//        }

        // Add the new data
        $security_deposit_amount = (float)$currentBookingRow['security_deposit_amount'];
        $security_deposit_paid = (float)$currentBookingRow['security_deposit_paid'];
        $security_deposit_total = $security_deposit_amount - $security_deposit_paid;
        $totalPriceFromBookingsData = (float)$currentBookingRow['total_price'];

        // Test 04: Total price verification
//        $totalPriceVerification =
//            $currentRowTotalRent + (float)$currentBookingRow['booking_fee_price'] + (float)$currentBookingRow['credit_card_fee_amount'] +
//            (float)$currentBookingRow['extras_price'] + (float)$currentBookingRow['security_deposit_amount']
//            - (float)$currentBookingRow['voucher'] - (float)$currentBookingRow['refund'] - (float)$currentBookingRow['third_party_agent'] - $security_deposit_total;
//
//        $totalPriceVerification = round($totalPriceVerification, 2);
//        if ($totalPriceFromBookingsData != $totalPriceVerification) {
//            echo 'Test 04 failed: Expected '.$totalPriceVerification.' | Found '.$totalPriceFromBookingsData.PHP_EOL;
//        }

//        if ($currentOwnerBookingID == '37051') {
//            var_dump($totalPriceFromBookingsData);
//            echo 'Booking 37051'.PHP_EOL;
//            var_dump(
//                $row['owner_due'],
//                $row['Commission Total']
//            );
//
//            echo 'DETAILS...'.PHP_EOL;
//            var_dump(
//                $currentRowTotalRent,
//                $currentBookingRow['booking_fee_price'],
//                $currentBookingRow['credit_card_fee_amount'],
//                $currentBookingRow['extras_price'],
//                $currentBookingRow['security_deposit_amount'],
//                $currentBookingRow['voucher'],
//                $currentBookingRow['refund'],
//                $currentBookingRow['third_party_agent'],
//                $security_deposit_total
//            );
//            die;
//        }

        $row['BookingRef'] = $currentBookingRow['bookingID'];
        $row['Rent'] = $currentRowTotalRent;
        $row['Guest Total'] = $totalPriceFromBookingsData;

        $row['Booking Fee Gross (inc vat)'] = (float)$currentBookingRow['booking_fee_price'];
        $row['Security Deposit'] = $security_deposit_amount;
        $row['Security Deposit (paid)'] = $security_deposit_paid;
        $row['Security Deposit Total'] = $security_deposit_total;

        $row['Settled to Owner'] = (float)$currentBookingRow['paid_to_owner'];
        $row['Owner deposit due'] = (float)$currentBookingRow['due_to_owner'] - $row['Settled to Owner'];

        $row['Booking Type'] = $currentBookingRow['bookingstatus'];
        $row['Owner Reference'] = '--';
        $row['Property reference'] = $row['PropertyRef'];
        $row['Property name'] = '--';

        $row['Holiday booked date'] = $currentBookingRow['bookingdate'];
        $row['Holiday start date'] = $currentBookingRow['bookingstartdate'];
        $row['Holiday end date'] = $currentBookingRow['bookingenddate'];

        $row['Source ID'] = $currentBookingRow['sourceID'];

        $data[] = $row;
    }

    bookingsReportStep3($data);
}


/**
 * STEP 03
 * Adds up additional columns to the bookings-finance report
 *
 * Reads from:
 * - export-Payments File
 */
function bookingsReportStep3($financesPerBooking) {
    $payments_filePath = __DIR__ . '/../imports/sykes-Lake-District-Lodge-Holidays-export-booking-payments_2021-02-27.csv';

    $paymentsFieldsToExtract = [
        'paymentdate',
        'paymenttotal',
    ];
    $customArrayKey = 'bookingID';
    $options = 'group';
    $bookingPaymentsData = getDataFromFile($payments_filePath, $paymentsFieldsToExtract, $customArrayKey, $options);

    $data = [];
    foreach ($financesPerBooking as $row) {
        $currentBookingID = $row['BookingRef'];
        $currentBookingPaymentsRow = $bookingPaymentsData[$currentBookingID] ?? [];

        $customerPayments = 0.0;
        $customerRefunds = 0.0;
        $customerSecurityDepositRefunds = 0.0;
        foreach ($currentBookingPaymentsRow as $paymentRow => $paymentColumn) {
            if (($currentPayment = (float)$paymentColumn['paymenttotal']) >= 0) {
                $customerPayments += $currentPayment;
            } else {
                // = IF there's REFUNDS after the Booking_End_Date AND the VALUE matches "Security Deposit (paid)" THEN don't add up to THIS column
                // ==> Show the previous logic in a separate column
                if ($paymentColumn['paymentdate'] >= $row['Holiday end date'] && $currentPayment === ($row['Security Deposit (paid)'] * -1)) {
//                    echo 'Found refund for booking '.$row['BookingRef'].' after the Booking_End_date.'.PHP_EOL.
//                    '  - Payment Date: '.$paymentColumn['paymentdate'].PHP_EOL.
//                    '  - Holiday end date: '.$row['Holiday end date'].PHP_EOL.
//                    '  - Current Payment: '.$currentPayment.PHP_EOL.
//                    '  - Security Deposit (paid): '.(float)$row['Security Deposit (paid)'].PHP_EOL;
                    $customerSecurityDepositRefunds += $currentPayment;
                    continue;
                }

                $customerRefunds += $currentPayment;
            }
        }

        // Add the new data
        $row['Guest Balance'] = $customerPayments - $row['Guest Total'];
        $row['Value paid by customer'] = $customerPayments;
        $row['Value refunded to customer'] = $customerRefunds;
        $row['Security Deposit refunded to customer'] = $customerSecurityDepositRefunds;

        $data[] = $row;
    }

    bookingsReportStep4($data);
}


/**
 * STEP 04
 * Adds up additional columns to the bookings-finance report
 *
 * Reads from:
 * - export-Properties File
 */
function bookingsReportStep4($financesPerBooking) {
    $properties_filePath = __DIR__ . '/../imports/sykes-Lake-District-Lodge-Holidays-export-properties_2021-02-27.csv';

    $propertyFieldsToExtract = [
        'cottagename',
        'commission_rate',
        'managerID',
    ];
    $customArrayKey = 'cottageID';
    $bookingsData = getDataFromFile($properties_filePath, $propertyFieldsToExtract, $customArrayKey);

    $data = [];
    foreach ($financesPerBooking as $row) {
        $currentPropertyID = $row['Property reference'];

        $currentBookingPropertyRow = $bookingsData[$currentPropertyID] ?? [];

        // Update Property name
        $row['Property name'] = $currentBookingPropertyRow['cottagename'];
        // Add the new data
        $row['Owner Reference'] = $currentBookingPropertyRow['managerID'];
        $row['Commission Rate'] = $currentBookingPropertyRow['commission_rate'];

        $data[] = $row;
    }

    bookingsReportStep5($data);
}


/**
 * STEP 05
 * Reorders the columns within the final file and filters the data
 *
 * Reads from:
 * - STEP_4_bookingFinances+Payments+Properties
 */
function bookingsReportStep5($financesPerBooking) {
    $new_financesReportPath = __DIR__ . '/../exports/STEP_5_reorderReportFields_'.date('Ymd_Hi').'.csv';

    $totalRows = 0;
    $data = [];
    foreach ($financesPerBooking as $row) {
        // FILTER 1 - Only show bookings where the start data is after 02-10-2020
        // THIS FILTER HAS MOVED TO STEP 2
//        if ($row['Holiday end date'] < '2020-10-02') {
//            continue;
//        }

        // FILTER 2 - Only show bookings from these properties: 1042074 and 104289
//        if ($row['Property reference'] != '1042074' || $row['Property reference'] != '104289') {
//            continue;
//        }

        // FILTER 3 - Only show bookings that have "Fix the fells donation" > 0
//        if (empty($row['Fix the fells donation'])) {
//            continue;
//        }


        // Reorder fields
        $data[] = [
            $row['Booking ID'],
            $row['Rent'],
            '',
            '',
            $row['Guest Total'],
            $row['Booking Fee Gross (inc vat)'],
            $row['Security Deposit'],
            $row['Security Deposit (paid)'],
            $row['Security Deposit Total'],
            $row['Guest Balance'],
            $row['Commission'],
            $row['VAT on commission'],
            $row['Commission from Extras'],
            $row['VAT on Extras commission'],
            $row['Commission Total'],
            $row['Commission Rate'],
            $row['Settled to Owner'],
            $row['Owner deposit due'],
            $row['Value paid by customer'],
            $row['Value refunded to customer'],
            $row['Security Deposit refunded to customer'],
            $row['Booking Type'],
            $row['Owner Reference'],
            $row['Property reference'],
            $row['Property name'],
            $row['Holiday booked date'],
            $row['Holiday start date'],
            $row['Holiday end date'],
            $row['Source ID'],
            '------',
            $row['owner_due'],
            $row['booking_fee'],
            $row['booking_fee_vat'],
            $row['Fix the fells donation'],
            $row['Extras dogs'],
            $row['admin_fee'],
            $row['BookingRef'],
        ];

        $totalRows++;
    }

    echo $totalRows.' rows processed'.PHP_EOL;
    echo count($data).' lines on Data Array'.PHP_EOL;

    $newHeaders = [
        'Booking ID',
        'Rent',
        'Guest Extras Net',
        'Guest Extras VAT',
        'Guest Total',
        'Booking Fee Gross (inc vat)',
        'Security Deposit',
        'Security Deposit (paid)',
        'Security Deposit Total',
        'Guest Balance',
        'Commission',
        'VAT on commission',
        'Commission from Extras',
        'VAT on Extras commission',
        'Commission Total',
        'Commission Rate',
        'Settled to Owner',
        'Owner deposit due',
        'Value paid by customer',
        'Value refunded to customer',
        'Security Deposit refunded to customer',
        'Booking Type',
        'Owner Reference',
        'Property reference',
        'Property name',
        'Holiday booked date',
        'Holiday start date',
        'Holiday end date',
        'Source ID',
        '__ADDITIONAL_FIELDS__ ->',
        'owner_due',
        'booking_fee',
        'booking_fee_vat',
        'Fix the fells donation',
        'Extras dogs',
        'admin_fee',
        'BookingRef',
    ];

    saveDataToFile($new_financesReportPath, $newHeaders, $data);
}


/**
 * Validates the booking-finances files and checks if the commission adds up correctly
 */
function validateCommissions() {
    $filePath = __DIR__ . '/../imports/sykes-Lake-District-Lodge-Holidays-export-booking-finances_2021-01-21.csv';
    $exportedFile = __DIR__ . '/../exports/NEW_LDLH-booking-finances_SUM.csv';
    $csv = fopen($filePath, 'r');
    if (!$csv) {
        die('Could not open file: "'.$filePath.'"');
    }

    echo 'Reading file: "'.$filePath.'"'.PHP_EOL;

    $headers = fgetcsv($csv); // get rid of header line

    $errorsCount = 0;
    $failedConditions = [];
    while ($row = fgetcsv($csv)) {
        $row = array_combine($headers, $row);

        // owner due check
        $commissionPercentage = 0.0;
        $commissionVatPercentage = 0.0;

        // OWNER_DUE + COMMISSION_VAT + COMMISSION = TOTAL_RENT from the booking file

        $propertyCommission = getPropertyCommission($row['cottageID']);

        if (empty($row['propertyCommission'])) {
            $propertyCommission = 0.0;
        }

//        if ((float)$row['owner_due'] !== 0.0) {
//            $commissionPercentage = (float)$row['commission'] / (float)$row['owner_due'];
//        }
//        if ((float)$commission !== 0.0) {
//            $commissionVatPercentage = (float)$commission_vat / (float)$commission;
//        }

        if ($commissionPercentage !== $propertyCommission || $commissionVatPercentage !== COMMISSION_PERCENTAGE_THRESHOLD) {
            // If the $owner_bookingID then SUM the amounts
            $failedConditions['owner_bookingID'] =
                [
                    'PROPERTY_COMMISSION' => $propertyCommission,
                    'commissionPercentage' => $commissionPercentage,
                    'commissionVatPercentage' => $commissionVatPercentage,
//                    'owner_due' => $owner_due,
//                    'commission' => $commission,
//                    'commission_vat' => $commission_vat,
                ];

            $errorsCount++;
        }
    }

    fclose($csv);


    // Save a new file with
    // Owner_bookingID | cottageID | owner_due | commission | commission_vat

    // Then use this file to compare against the Bookings file on the TOTAL_RENT column
    // OWNER_DUE + COMMISSION_VAT + COMMISSION = TOTAL_RENT

//    if ($errorsCount >= MAX_SAMPLE_SIZE) {
//        echo 'Sample Size error reached: '.MAX_SAMPLE_SIZE.PHP_EOL;
//        print_r($failedConditions);
//        die();
//    }
}


/**
 * Calculates the commission and commission_vat for a given $ownerBookingID
 * @param $ownerBookingID
 * @return float[]
 */
function calculateCommission($ownerBookingID) {
    if (empty($ownerBookingID)) {
        echo PHP_EOL.'ERROR: Invalid ownerBookingID passed!'.PHP_EOL;
        return [];
    }

    // Read from file the first time

    $filePath = __DIR__ . '/../imports/sykes-Lake-District-Lodge-Holidays-export-booking-finances_2021-01-21.csv';
    $csv = fopen($filePath, 'r');
    if (!$csv) {
        die('Could not open file: "'.$filePath.'"');
    }

    fgetcsv($csv); // get rid of header line

    $commissionData = [
        'commission_vat' => 0.0,
        'commission' => 0.0,
    ];
    while ($row = fgetcsv($csv)) {
        list(
            $owner_bookingID,
            $paymentcaption,
            $invoice_date,
            $bookingstartdate,
            $cottageID,
            $owner_due,
            $commission_vat,
            $commission,
            $booking_fee,
            $booking_fee_vat,
            $admin_fee,
            $other_extras_net,
            $other_extras_vat
            ) = $row;

        if ((int)$owner_bookingID === (int)$ownerBookingID) {
            $commissionData['commission_vat'] += (float)$commission_vat;
            $commissionData['commission'] += (float)$commission;
        }
    }

    fclose($csv);

    // TODO if the file data is in memory, then read from array

    return $commissionData;
}


/**
 * Calculates the total commission for each booking and adds it to the report.
 * The commission is read from the export-booking-finances.
 */
function customReport() {
    $filePath = __DIR__ . '/../imports/Lake-District-Lodge-Holidays-requested-report-23-02-2021.csv';
    $exportedFile = __DIR__ . '/../exports/NEW_Lake-District-Lodge-Holidays-requested-report-23-02-2021.csv';
    $csv = fopen($filePath, 'r');
    if (!$csv) {
        die('Could not open file: "'.$filePath.'"');
    }

    $headerLine = fgetcsv($csv); // get rid of header line

    $recordCount = 0;
    $customReportData = [];
    while ($row = fgetcsv($csv)) {
        list(
            $owner_bookingID,
            $bookingID,
            $confirmed_date,
            $from_date,
            $bookingstatus,
            $brand,
            $booking_fee,
            $commission,
            $commission_vat,
            $party_size,
            $booking_type
            ) = $row;

        $calculatedCommission = calculateCommission($owner_bookingID);

        $customReportData[$owner_bookingID] = [
            'owner_bookingID' => $owner_bookingID,
            'bookingID' => $bookingID,
            'confirmed_date' => $confirmed_date,
            'from_date' => $from_date,
            'bookingstatus' => $bookingstatus,
            'brand' => $brand,
            'booking_fee' => (int)$booking_fee,
            'commission' => $calculatedCommission['commission'],
            'commission_vat' => $calculatedCommission['commission_vat'],
            'party_size' => $party_size,
            'booking_type' => $booking_type
        ];

        $recordCount++;
    }

    fclose($csv);


    echo 'Total records processed: '.$recordCount."\n";

    // Write data to the new file
    $fileStream = fopen($exportedFile, 'w');

    // Write header
    fputcsv($fileStream, $headerLine);

    // Write data
    foreach ($customReportData as $paymentData) {
        fputcsv($fileStream, $paymentData);
    }
    fclose($fileStream);

    echo 'Finished writing file';
}


function instructions() {
    echo 'Please pass on a function name to execute!'.PHP_EOL.
        '  - validateBookings'.PHP_EOL.
        '  - processPayments'.PHP_EOL.
        '  - calculateCommission OWNER_BOOKING_ID'.PHP_EOL.
        '  - validateCommissions'.PHP_EOL.
        '  - customReport'.PHP_EOL.
        '  - getPropertyCommission PROPERTY_ID'.PHP_EOL.
        '  - bookingsReport'.PHP_EOL.
        PHP_EOL;
}

echo "Set memory_limit to 1024M: previous was ".ini_set('memory_limit', '1024M');
echo "\nSet time limit to 0: ".set_time_limit(0);
echo "\n\n";

if (!empty($argv) && $argc >= 2) {
    switch ($argv[1]) {
        case 'validateBookings':
            validateBookings();
            break;
        case 'processPayments':
            processPayments();
            break;
        case 'calculateCommission':
            print_r(calculateCommission($argv[2] ?? 0));
            break;
        case 'validateCommissions':
            validateCommissions();
            break;
        case 'customReport':
            customReport();
            break;
        case 'getPropertyCommission':
            print_r(getPropertyCommission($argv[2] ?? 0));
            break;
        case 'bookingsReport':
            bookingsReportStep1();
            break;
        default:
            echo 'Option not found!'.PHP_EOL.PHP_EOL;
            instructions();
            break;
    }
} else {
    instructions();
}
