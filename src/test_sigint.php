<?php
pcntl_async_signals(true);
pcntl_signal(SIGINT, "endCommand");
echo "starting command\n";
$dateStart = new DateTime();
echo $dateStart->format('Y-m-d H:i') . PHP_EOL;
echo "Press control c to finish".PHP_EOL;
$run = true;
while($run) {
    sleep(5);
    $runningDate = $dateStart->diff(new DateTime())->format('%i hours %s seconds');
    echo "waiting $runningDate".PHP_EOL;
}
echo "ending\n";
echo "Time taken\n";
$dateEnd = new DateTime();
$intervalDiff = $dateStart->diff($dateEnd);
print_r(
    $intervalDiff->format('%d days, %i hours %s seconds')
);

function endCommand() {
    $GLOBALS['run'] = false;
}

